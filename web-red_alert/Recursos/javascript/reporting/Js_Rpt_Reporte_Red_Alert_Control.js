﻿$(document).on('ready', function () {
    _inicializar_pagina();


});
function _inicializar_pagina() {
    try {
        _estado_inicial();
        _boton_imprimir_control_alertas();
        _boton_busqueda();
        _crear_tbl_alertas_rojas();
        _load_areas('#cmb_busqueda_por_area');
        _load_numero_partes('#cmb_busqueda_por_numero_parte');
        _load_estatus('#cmb_busqueda_por_estatus');
        _load_clientes('#cmb_busqueda_por_cliente');

    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
}
function _estado_inicial() {
    try {
        $('#dtp_fecha_inicio').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'en',
            format: "DD/MM/YYYY"
        });

        $("#dtp_fecha_inicio").datetimepicker("useCurrent", true);

        $('#dtp_fecha_termino').datetimepicker({
            defaultDate: new Date(),
            viewMode: 'days',
            locale: 'en',
            format: "DD/MM/YYYY"
        });

        $("#dtp_fecha_termino").datetimepicker("useCurrent", true);

    } catch (ex) {
        _mostrar_mensaje("Technical report", ex.message)
    }
}
function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'en',
        closeButton: true,
        buttons: [{
            label: 'Close',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}

function _boton_imprimir_control_alertas() {
    $('#btn_imprimir_control_alertas').on('click', function (e) {
        e.preventDefault();
        bootbox.confirm({
            title: 'Print Report Red Alert Control',
            message: 'Are you sure to print the report?',
            callback: function (result) {
                if (result) {
                    _Imprimir_reporte_control_alertas();
                }
            }
        });
    });
}

function _boton_busqueda() {
    $('#btn_busqueda').on('click', function (e) {
        e.preventDefault();
        _search_alertas_rojas_por_filtros();
    });
}

function _Imprimir_reporte_control_alertas() {
    var alerta = null;
    var isComplete = false;

    try {

        alerta = new Object();

        alerta.Fecha_Creo = parseDateIntercambiarDiaMes($('#f_inicio_1').val());
        alerta.Fecha_Creo1 = parseDateIntercambiarDiaMes($('#f_termino_1').val());

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Reporte_Control_Alertas',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);

                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "No results were found"); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
    return isComplete;
}

function parseDateIntercambiarDiaMes(dateString) {
    //Intercambia el dia y el mes de los formatos de fecha( DD/MM/YYYY o MM/DD/YYYY )
    var dateTime = dateString.split(" ");
    var dateOnly = dateTime[0];
    var dates = dateOnly.split("/");
    var temp = dates[1] + "/" + dates[0] + "/" + dates[2];
    return temp;
}

function _crear_tbl_alertas_rojas() {
    try {
        $('#tbl_alertas_rojas').bootstrapTable('destroy');
        $('#tbl_alertas_rojas').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,
            columns: [
                { field: 'No_Alerta_Roja', title: 'Red Alert Number', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Cliente', title: 'Customer', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Numero_Parte', title: 'Part Number', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Area', title: 'Affected Area', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Descripcion_1', title: 'What happened?', width: '30%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                {
                    field: 'Estatus', title: 'Status', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {
                        var color = '';
                        switch (value) {
                            case 'CANCELED':
                                color = "black";
                                break;
                            case 'OPENED':
                                color = "yellow";
                                break;
                            case 'PROCESS':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'CLOSED':
                                color = "green";
                                break;
                            case 'REJECTED':
                                color = "rgba(221, 24, 24, 1)";
                                break;
                            case 'AUTHORIZED':
                                color = 'rgba(3, 251,16, 1)';
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }
                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
                {
                    field: 'View', title: 'Report', align: 'center', width: '3%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _visualizar" id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) +
                                    '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-list-alt" title=""></i></a>' +
                               '</div>';
                    }
                }
            ]
        });
    } catch (e) {

    }
}
function btn_visualizar_click(renglon) {
    var row = $(renglon).data('alerta_roja');
    var Alerta = null;
    var isComplete = false;

    try {

        Alerta = new Object();
        Alerta.No_Alerta_Roja = row.No_Alerta_Roja;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Reporte_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);

                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "No results found"); }
            }
        });

    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    }
    return isComplete;
}
function _load_areas(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Area',
            allowClear: true,
            ajax: {
                url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Consultar_Areas',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_numero_partes(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Part Number',
            allowClear: true,
            tags: true,
            ajax: {
                url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Consultar_Numero_Partes',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_estatus(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Status',
            allowClear: true,
            ajax: {
                url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Consultar_Estatus',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_clientes(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Customer',
            allowClear: true,
            ajax: {
                url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Consultar_Clientes',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _search_alertas_rojas_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();


        if ($('#cmb_busqueda_por_cliente :selected').val() !== undefined && $('#cmb_busqueda_por_cliente:selected').val() !== '')
            filtros.Cliente_ID = parseInt($('#cmb_busqueda_por_cliente :selected').val());

        if ($.trim($('#txt_busqueda_por_descripcion_1').val()) !== '')
            filtros.Descripcion_1 = $('#txt_busqueda_por_descripcion_1').val();

        if ($('#cmb_busqueda_por_area :selected').val() !== undefined && $('#cmb_busqueda_por_area:selected').val() !== '')
            filtros.Area_ID = parseInt($('#cmb_busqueda_por_area :selected').val());

        if ($('#cmb_busqueda_por_estatus :selected').val() !== undefined && $('#cmb_busqueda_por_estatus :selected').val() !== '')
            filtros.Estatus_ID = parseInt($('#cmb_busqueda_por_estatus :selected').val());

        if ($('#cmb_busqueda_por_numero_parte :selected').val() !== undefined && $('#cmb_busqueda_por_numero_parte :selected').val() !== '')
            filtros.Numero_Parte_ID = parseInt($('#cmb_busqueda_por_numero_parte :selected').val());
        if (isNaN(filtros.Numero_Parte_ID)) {
            filtros.Numero_Parte_ID = 0;
            filtros.Numero_Parte = $('#cmb_busqueda_por_numero_parte :selected').val();
        }
        
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Rpt_Reporte_Red_Alert_Controller.asmx/Consultar_Alertas_Rojas',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_alertas_rojas').bootstrapTable('load', JSON.parse(datos.d));
                    $('#tbl_alertas_rojas').css('display', 'block');
                    _init_tbl_btn_config('._visualizar', 'Alert Report', 'bottomRight');
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _init_tbl_btn_config(classCss, text, alineacionTooltip) {
    $(classCss).each(function () {
        $(this).qtip({
            content: text,
            position: {
                corner: {
                    target: 'topMiddle',
                    tooltip: alineacionTooltip
                }
            },
            show: {
                when: { event: 'mouseover' },
                ready: false
            },
            hide: { event: 'mouseout' },
            style: {
                border: {
                    width: 5,
                    radius: 7
                },
                padding: 5,
                textAlign: 'center',
                tip: {
                    corner: true,
                    method: "polygon",
                    border: 1,
                    height: 20,
                    width: 9
                },
                background: '#F5F6CE',
                color: '#2d2d30',
                width: 100,
                'font-size': 'small',
                'font-family': 'Calibri',
                'font-weight': 'Bold',
                tip: true,
                name: 'blue'
            }
        });
    });
}
﻿<%@ Page Title="Operación Inspecciones" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.Master" AutoEventWireup="true" CodeBehind="Frm_Ope_Inspecciones.aspx.cs" Inherits="web_red_alert.Paginas.Operacion.Frm_Ope_Inspecciones" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <link href="../../Recursos/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <link href="../../Recursos/estilos/demo_form.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-combo/select2.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table-current/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../Recursos/icon-picker/css/icon-picker.css" rel="stylesheet" />
    <style>
        .fixed-table-toolbar .bars, .fixed-table-toolbar .search, .fixed-table-toolbar .columns {
            margin-top: 0px !important;
            color:red;
        }

        .form-control-icono {
            /*Color de fondo de los controles*/
            background-color: inactiveborder;
            height: 25px;
            font-size: 90%;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
    <link href="../../Recursos/estilos/css_producto.css" rel="stylesheet" />
    <script src="../../Recursos/plugins/parsley.js"></script>
    <script src="../../Recursos/bootstrap-table-current/bootstrap-table.js"></script>
    <script src="../../Recursos/plugins/jquery.formatCurrency-1.4.0.min.js"></script>
    <script src="../../Recursos/plugins/jquery.formatCurrency.all.js"></script>
    <script src="../../Recursos/plugins/accounting.min.js"></script>
    <script src="../../Recursos/plugins/pinch.js"></script>
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <script src="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
    <script src="../../Recursos/bootstrap-table-current/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../Recursos/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-editable.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>
    <script src="../../Recursos/icon-picker/js/iconPicker.js"></script>
    <script src="../../Recursos/javascript/seguridad/Js_Controlador_Sesion.js"></script>
    <script src="../../Recursos/plugins/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script src="../../Recursos/bootstrap-combo/select2.js"></script>
    <script src="../../Recursos/plugins/URI.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpJMhd5A-jAGTq9ICV61eexuA1oOhs8CY&libraries=geometry"></script>
    <script src="../../Recursos/javascript/operacion/Js_Ope_Inspecciones.js"></script>

    <style>
        #tbl_ordenes_red_alert thead tr th {
            border: none !important;
        }

            #tbl_ordenes_red_alert thead tr th:nth-child(n+4) {
                border-left: 2px solid #ddd !important; 
                border-right: 2px solid #ddd !important;
            }

             #tbl_lista_comentarios{
                 border-bottom-color:red;
                 border-top-color:red;
                 
             }
        .select2-container {
            width: 100% !important;
        }

        .search input:first-of-type {
            min-width: 200px !important;
        }

        #lbl_observaciones {
            color: red;
            display: none;
            font-size: large;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="div_principal_alertas_rojas">

        <div class="container-fluid" style="height: 100vh;">
            <div class="row">
                <div class="col-sm-12 text-left" style="background-color: white!important;">
                    <h3>Inspecciones</h3>
                </div>
            </div>

            <hr />
            <div class="panel panel-color panel-info collapsed" id="panel1">
                <div class="panel-heading filter">
                    <h3 class="panel-title">
                        <i style="color: white;" class="glyphicon glyphicon-filter"></i>&nbsp;Filtros de búsqueda
                    </h3>
                    <div class="panel-options">
                        <a id="ctrl_panel" href="#" data-toggle="panel">
                            <span class="collapse-icon">–</span>
                            <span class="expand-icon">+</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>No. Inspección</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_no_inspeccion" class="form-control" placeholder="Buscar por No. Inspección" />
                        </div>
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>UUID</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_dispositivo" class="form-control" placeholder="Buscar por UUID" />
                        </div>
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label> Código de Barras</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_codigo" class="form-control" placeholder="Buscar por Código de Barras" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1" style="margin-top: 8px;">
                            <%--<label>Status</label>--%>
                        </div>
                        <div class="col-md-3">
                          <%--  <select id="cmb_busqueda_por_estatus" class="form-control"></select>--%>
                        </div>
                        <div class="col-md-8" align="right">
                            <button type="button" id="btn_busqueda" class="btn btn-secondary btn-icon btn-icon-standalone btn-lg">
                                <i class="fa fa-search"></i>
                                <span>Buscar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <hr />
           <%-- <div id="toolbar" style="margin-left: 5px;">
                <div class="btn-group" role="group" style="margin-left: 5px;">
                    <button id="btn_inicio" type="button" class="btn btn-danger btn-sm" title="Inicio"><i class="glyphicon glyphicon-home"></i></button>
                    <button id="btn_nuevo" type="button" class="btn btn-danger btn-sm" title="Nuevo"><i class="glyphicon glyphicon-plus"></i></button>
                </div>
            </div>--%>
            <table id="tbl_inspecciones" data-toolbar="#toolbar" class="table table-responsive"></table>
        </div>

    </div>
    <div id="div_Informacion" style="display: none">
        <div id="div_orden_cambio_proceso">
            <div class="container-fluid" style="height: 100vh;">
                <div id="div_solo_informacion">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-8 text-left" style="background-color: white!important;">
                                <h3>Inspecciones</h3>
                            </div>
                            <div class="row">
                                <div class="col-sm-12"  style="text-align:right; float:right;" >
                                    <%-- <li style="list-style:none; float:right;">--%>
                                    <button id="btn_salir" type="button" class="btn btn-danger btn-sm" title="">
                                        <i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Cancel Red Alert
                                    </button>

                                    <button id="btn_guardar" type="button" class="btn btn-danger btn-sm" title="">
                                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;&nbsp;Save Red Alert
                                    </button>
                                     
                                    <button id="btn_guardar_comentarios" type="button" class="btn btn-danger btn-sm" title="">
                                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;&nbsp;Save Comment
                                    </button>
                                        <%-- </li>--%>
                                    <button id="btn_regresar" type="button" class="btn btn-danger btn-sm" title="" style="display: none;">
                                        <i class="glyphicon glyphicon-share-alt"></i>&nbsp;&nbsp;Regresar
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <%--Contenedor de rows de formulario--%>
                    <div class="row">
                        <label id="lbl_observaciones"></label>
                    </div>

                     <div class="row">
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_inspeccion_id" class="fuente_lbl_controles">Inspección ID:</label>
                            <input type="text" id="txt_inspeccion_id" class="form-control" placeholder="Inspección ID" />
                         
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_dispositivo_id" class="fuente_lbl_controles">UUID:</label>
                            <input type="text" id="txt_dispositivo_id" class="form-control" placeholder="UUID" />
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_latitud" class="fuente_lbl_controles">Latitud:</label>
                            <input type="text" id="txt_latitud" class="form-control" placeholder="Latitud" />
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_longitud" class="fuente_lbl_controles">Longitud:</label>
                            <input type="text" id="txt_longitud" class="form-control" placeholder="Longitud" />
                        </div>
                    </div>
                
                      <div class="row">
                        <div class="col-md-3 col-xs-3">
                           <label for="txt_nivel_bateria" class="fuente_lbl_controles">Nivel Batería:</label>
                            <input type="text" id="txt_nivel_bateria" class="form-control" placeholder="Nivel de Batería" />
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_codigo_barras" class="fuente_lbl_controles">Código de Barras:</label>
                            <input type="text" id="txt_codigo_barras" class="form-control" placeholder="Código de Barras" />
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <label for="txt_dispositivo" class="fuente_lbl_controles">Dispositivo:</label>
                            <input type="text" id="txt_dispositivo" class="form-control" placeholder="Dispositivo" />
                        </div>
                        <div class="col-md-3 col-xs-3">
                           <label class="text-bold text-left text-medium fuente_lbl_controles" title="Fecha" style="margin-bottom: 5px !important;">Fecha:</label>
                            <div class="input-group date" id="dtp_fecha_inicio_cambio">
                                <input type="text" id="f_inicio" class="form-control" style="margin: 0px;" placeholder="dd/mm/aaaa" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>    
                            </div>   
                        </div>
                    </div>
                 <%--   <div class="row">
                        <div class="col-md-3 col-xs-3">
                           
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <%--<label for="cmb_turnos" class="fuente_lbl_controles">(*) Related Shift:</label>
                            <select id="cmb_turnos" class="form-control"></select>--%>
                       <%-- </div>
                        <div class="col-md-3 col-xs-3">
                           <%-- <label for="txt_numero_car" class="fuente_lbl_controles">(*) CAR #:</label>
                            <input id="txt_numero_car" type="text" class="form-control" placeholder="CAR #" />--%>
                       <%-- </div>
                        <div class="col-md-3 col-xs-3">
                            <%--<label for="cmb_estatus" class="fuente_lbl_controles">(*) Status:</label>
                            <select id="cmb_estatus" class="form-control"></select>--%>
                    <%--    </div>
                    </div>--%>
                      <div class="row">
                              <h5>Ubicación</h5>
                              <div id="divmapa" style="position: relative;width: 100%; height: 600px; overflow:visible; " ></div>
                        </div>
                    <div class="page-header">
                        &nbsp;
                    </div>
                    <div class="page-header">
                        <div class="row">
                              <h5>Acciones</h5>
                            <table id="tbl_lista_acciones" class="table table-responsive"></table>  
                            <div id="div_acciones">                            
                            </div>
                        </div>
                    </div>
                    
                  

                   <%-- <div class="row">
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">What happened?</label>
                            <input type="text" id="txt_descripcion_1" class="form-control input-sm" placeholder="What happened?" data-parsley-required="true" maxlength="250" />
                            <%-- style="min-height: 50px !important;" --%>
                       <%-- </div>
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">Why is it a problem?</label>
                            <input type="text" id="txt_descripcion_2" class="form-control input-sm" placeholder="Why is it a problem?" data-parsley-required="true" maxlength="250" />
                        </div>
                    </div>--%>
                   <%-- <div class="row">
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">When did it happen?</label>
                            <input type="text" id="txt_descripcion_3" class="form-control input-sm" placeholder="When did it happen?" data-parsley-required="true" maxlength="250" />
                        </div>
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">Who has detected it?</label>
                            <input type="text" id="txt_descripcion_4" class="form-control input-sm" placeholder="Who has detected it?" data-parsley-required="true" maxlength="250" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">Where has it been detected?</label>
                            <input type="text" id="txt_descripcion_5" class="form-control input-sm" placeholder="Where has it been detected?" data-parsley-required="true" maxlength="250" />
                        </div>
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">How has it been detected?</label>
                            <input type="text" id="txt_descripcion_6" class="form-control input-sm" placeholder="How has it been detected?" data-parsley-required="true" maxlength="250" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">How many?</label>
                            <input type="text" id="txt_descripcion_7" class="form-control input-sm" placeholder="How many?" data-parsley-required="true" maxlength="250" />
                        </div>
                    </div>--%>
                   <%-- <div class="row">
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">Good condition</label>
                            <input id="fl_doc_condicion_buena" style="width: 100%;" type="file" name="file" accept=".png,.jpeg, .gif,.jpg" /><br />
                            <button id="btn_agregar_imagen_condicion_buena" type="button" class="btn btn-danger btn-sm" title="Add picture" style="margin: 0px; padding: 4px 6px;">
                                <i class="fa fa-plus"></i>&nbsp;&nbsp;Attach picture
                            </button>
                            <br />
                            <br />
                            <img id="imagen_condicion_buena" class="img-responsive" alt="Good condition picture" height="300" width="300" />
                        </div>
                        <div class="col-md-6">
                            <label class="fuente_lbl_controles">Bad condition</label>
                            <input id="fl_doc_condicion_mala" style="width: 100%;" type="file" name="file" accept=".png,.jpeg, .gif,.jpg" /><br />
                            <button id="btn_agregar_imagen_condicion_mala" type="button" class="btn btn-danger btn-sm" title="Add picture" style="margin: 0px; padding: 4px 6px;">
                                <i class="fa fa-plus"></i>&nbsp;&nbsp;Attach picture
                            </button>
                            <br />
                            <br />
                            <img id="imagen_condicion_mala" class="img-responsive" alt="Bad condition picture" height="300" width="300" />

                        </div>
                    </div>--%>
                  <%--  <div class="page-header">
                        &nbsp;
                    </div>--%>
                   <%-- <div class="row">
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Number of rejected parts</label>
                            <input type="text" id="txt_estatus_partes_1" class="form-control input-sm" placeholder="Rejected parts" data-parsley-required="true" maxlength="10" />
                        </div>
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Number of inspected parts</label>
                            <input type="text" id="txt_estatus_partes_2" class="form-control input-sm" placeholder="Inspected parts" data-parsley-required="true" maxlength="10" />
                        </div>
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Number of produced parts</label>
                            <input type="text" id="txt_estatus_partes_3" class="form-control input-sm" placeholder="Produced parts" data-parsley-required="true" maxlength="10" />
                        </div>
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Number of suspected parts</label>
                            <input type="text" id="txt_estatus_partes_4" class="form-control input-sm" placeholder="Suspected parts" data-parsley-required="true" maxlength="10" />
                        </div>
                    </div>--%>
                   <%-- <div class="row">
                        <div class="col-md-12">
                            <label class="fuente_lbl_controles">Describe containment inspection method in house/customer</label>
                            <input type="text" id="txt_estatus_descripcion" class="form-control input-sm" placeholder="Description" data-parsley-required="true" maxlength="250" />
                        </div>
                    </div>--%>
                   <%-- <div class="row">
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Corrective actions initiated?</label>
                            <select id="cmb_estatus_condicion_1" class="form-control input-sm" data-parsley-required="true">
                                <option value="">--SELECT--</option>
                                <option value="YES">YES</option>
                                <option value="NO">NO</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Customer afectation?</label>
                            <select id="cmb_estatus_condicion_2" class="form-control input-sm" data-parsley-required="true">
                                <option value="">--SELECT--</option>
                                <option value="YES">YES</option>
                                <option value="NO">NO</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="fuente_lbl_controles">Other AAM plant afected?</label>
                            <select id="cmb_estatus_condicion_3" class="form-control input-sm" data-parsley-required="true">
                                <option value="">--SELECT--</option>
                                <option value="YES">YES</option>
                                <option value="NO">NO</option>
                            </select>
                        </div>
                    </div>--%>
                </div>                  
               

            </div>
        </div>
      
    </div>

    
</asp:Content>

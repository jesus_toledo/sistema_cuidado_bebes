﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.Master" AutoEventWireup="true" CodeBehind="Frm_Ope_Autorizacion_Alertas.aspx.cs" Inherits="web_red_alert.Paginas.Operacion.Frm_Ope_Autorizacion_Alertas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Recursos/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <link href="../../Recursos/estilos/demo_form.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-combo/select2.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table-current/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../Recursos/icon-picker/css/icon-picker.css" rel="stylesheet" />
    <style>
        .fixed-table-toolbar .bars, .fixed-table-toolbar .search, .fixed-table-toolbar .columns {
            margin-top: 0px !important;
        }

        .form-control-icono {
            /*Color de fondo de los controles*/
            background-color: inactiveborder;
            height: 25px;
            font-size: 90%;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
    <link href="../../Recursos/estilos/css_producto.css" rel="stylesheet" />
    <script src="../../Recursos/plugins/parsley.js"></script>
    <script src="../../Recursos/bootstrap-table-current/bootstrap-table.js"></script>

    <script src="../../Recursos/plugins/jquery.formatCurrency-1.4.0.min.js"></script>   
    <script src="../../Recursos/plugins/jquery.formatCurrency.all.js"></script>
    <script src="../../Recursos/plugins/accounting.min.js"></script>
    <script src="../../Recursos/plugins/pinch.js"></script>

    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <script src="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
    <script src="../../Recursos/bootstrap-table-current/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../Recursos/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-editable.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>
    <script src="../../Recursos/icon-picker/js/iconPicker.js"></script>
    <script src="../../Recursos/javascript/seguridad/Js_Controlador_Sesion.js"></script>
    <script src="../../Recursos/plugins/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script src="../../Recursos/bootstrap-combo/select2.js"></script>
    <script src="../../Recursos/bootstrap-combo/es.js"></script>
    <script src="../../Recursos/plugins/URI.min.js"></script>
    <script src="../../Recursos/javascript/operacion/Js_Ope_Autorizacion_Alertas.js"></script>

    <style>
        #tbl_ordenes_red_alert thead tr th{
            border: none !important;
        }
        #tbl_ordenes_red_alert thead tr th:nth-child(n+4){
            border-left: 2px solid #ddd !important;
            border-right: 2px solid #ddd !important;
        }
        .select2-container {
            width: 100% !important;
        }
         .search input:first-of-type {
            min-width: 200px !important;
        }
             #lbl_observaciones {
            color:red;
            display:block;
            font-size:large;
            text-align:left;

        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="div_principal_alertas_rojas">
        
        <div class="container-fluid" style="height: 100vh;">
            <div class="row">
                <div class="col-sm-12 text-left" style="background-color: white!important;">
                    <h3>Authorize Alerts</h3>
                </div>
            </div>
            <hr />  
            <%--<hr />--%>
            <div  id="toolbar" style="margin-left: 5px;">
                <div class="btn-group" role="group" style="margin-left: 5px;">
                    <button id="btn_inicio" type="button" class="btn btn-danger btn-sm" title="Inicio"><i class="glyphicon glyphicon-home"></i></button>                 
                </div>
            </div>
            <table id="tbl_alertas_rojas" data-toolbar="#toolbar" class="table table-responsive"></table>
        </div>

    </div>
    <div id="div_Informacion" style="display:none">
        <div id="div_orden_cambio_proceso">
            <div class="container-fluid" style="height: 100vh;">
                <div id="div_solo_informacion">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-8 text-left" style="background-color: white!important;">
                                <h3>Red Alert</h3>
                            </div>
                                <div class="row">
                                    <div class="col-sm-12" style="text-align:right; float:right;">
                                        <button id="btn_salir" type="button" class="btn btn-danger btn-sm" title="">
                                            <i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Cancel Red Alert
                                        </button>

                                        <button id="btn_guardar" type="button" class="btn btn-danger btn-sm" title="">
                                            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;&nbsp;Save Red Alert
                                        </button>
                                            
                                    <button id="btn_guardar_comentarios" type="button" class="btn btn-danger btn-sm" title="">
                                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;&nbsp;Save Comment
                                    </button>

                                        <button id="btn_regresar" type="button" class="btn btn-danger btn-sm" title="" style="display:none;">
                                            <i class="glyphicon glyphicon-share-alt"></i>&nbsp;&nbsp;Regresar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--Contenedor de rows de formulario--%>   
                    <div class="row">
                         <label id="lbl_observaciones"></label>     
                      </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_plantas" class="fuente_lbl_controles">(*) AAM Plant:</label>
                                 <select id="cmb_plantas" class="form-control"></select>
                                <input type="text" id="txt_no_alerta_roja" style="display:none" />
                            </div>
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_unidades_negocio" class="fuente_lbl_controles">(*) Bussines Unit:</label>
                                 <select id="cmb_unidades_negocio" class="form-control"></select>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_productos" class="fuente_lbl_controles">(*) Product Name:</label>
                                 <select id="cmb_productos" class="form-control"></select>                                   
                            </div>
                             <div class="col-md-3 col-xs-3">
                                 <label for="cmb_numero_partes" class="fuente_lbl_controles">(*) Part Number:</label>
                                 <select id="cmb_numero_partes" class="form-control" ></select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_clientes" class="fuente_lbl_controles">(*) Customer:</label>
                                   <select id="cmb_clientes" class="form-control"></select>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="txt_referencia_productos" class="fuente_lbl_controles">Product Reference:</label>
                                   <input type="text" id="txt_referencia_productos" class="form-control" placeholder="Product Reference"/>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="txt_sitios_clientes" class="fuente_lbl_controles">Customer Site:</label>
                                   <input type="text" id="txt_sitios_clientes" class="form-control" placeholder="Customer Site"/>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="txt_vehiculos" class="fuente_lbl_controles">(*) Product Model:</label>
                                   <input type="text" id="txt_vehiculos" class="form-control" placeholder="Product Model"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_areas" class="fuente_lbl_controles">(*) Afected Area:</label>
                                   <select id="cmb_areas" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_turnos" class="fuente_lbl_controles">(*) Related Shift:</label>
                                   <select id="cmb_turnos" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="txt_numero_car" class="fuente_lbl_controles">(*) CAR #:</label>
                                   <input id="txt_numero_car" type="text" class="form-control" placeholder="CAR #"/>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_estatus" class="fuente_lbl_controles">(*) Status:</label>
                                   <select id="cmb_estatus" class="form-control"></select>                                
                            </div>
                        </div>
                        <div class="page-header">
                            &nbsp;
                        </div>
                        <div class="page-header">
                            <div class="row">
                                <div id="div_circunstancias">
                                    <%--Criterios parametrizables--%>
                                </div> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">What happened?</label>
                                <input type="text" id="txt_descripcion_1" class="form-control input-sm" placeholder="What happened?" data-parsley-required="true" maxlength="250"/> <%-- style="min-height: 50px !important;" --%>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Why is it a problem?</label>
                                <input type="text" id="txt_descripcion_2" class="form-control input-sm" placeholder="Why is it a problem?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">When did it happen?</label>
                                <input type="text" id="txt_descripcion_3" class="form-control input-sm" placeholder="When did it happen?" data-parsley-required="true" maxlength="250"/>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Who has detected it?</label>
                                <input type="text" id="txt_descripcion_4" class="form-control input-sm" placeholder="Who has detected it?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Where has it been detected?</label>
                                <input type="text" id="txt_descripcion_5" class="form-control input-sm" placeholder="Where has it been detected?" data-parsley-required="true" maxlength="250"/>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">How has it been detected?</label>
                                <input type="text" id="txt_descripcion_6" class="form-control input-sm" placeholder="How has it been detected?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">How many?</label>
                                <input type="text" id="txt_descripcion_7" class="form-control input-sm" placeholder="How many?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Goog condition</label>
                                 <input id="fl_doc_condicion_buena" style="width: 100%;" type="file" name="file" accept=".png,.jpeg, .gif,.jpg"/><br/>
                                <button id="btn_agregar_imagen_condicion_buena" type="button" class="btn btn-danger btn-sm" title="Add picture" style="margin: 0px; padding: 4px 6px;">
								  <i class="fa fa-plus"></i>&nbsp;&nbsp;Attach picture
								</button>   
                                 <br/><br/>
                                <img id="imagen_condicion_buena" class="img-responsive" alt="Good condition picture" height="300" width="300"/>          
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Bad condition</label>
                                  <input id="fl_doc_condicion_mala" style="width: 100%;" type="file" name="file" accept=".png,.jpeg, .gif,.jpg"/><br/>
                                 <button id="btn_agregar_imagen_condicion_mala" type="button" class="btn btn-danger btn-sm" title="Add picture" style="margin: 0px; padding: 4px 6px;">
								   <i class="fa fa-plus"></i>&nbsp;&nbsp;Attach picture
							     </button>
                                 <br/><br/>
                               <img id="imagen_condicion_mala" class="img-responsive" alt="Bad condition picture" height="300" width="300"/>
                            </div>
                        </div>
                        <div class="page-header">
                            &nbsp;
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Number of rejected parts</label>
                                <input type="text" id="txt_estatus_partes_1" class="form-control input-sm" placeholder="Rejected parts" data-parsley-required="true" maxlength="10" />
                            </div>
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Number of inspected parts</label>
                                <input type="text" id="txt_estatus_partes_2" class="form-control input-sm" placeholder="Inspected parts" data-parsley-required="true" maxlength="10" />
                            </div>
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Number of produced parts</label>
                                <input type="text" id="txt_estatus_partes_3" class="form-control input-sm" placeholder="Produced parts" data-parsley-required="true" maxlength="10" />
                            </div>
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Number of suspected parts</label>
                                <input type="text" id="txt_estatus_partes_4" class="form-control input-sm" placeholder="Suspected parts" data-parsley-required="true" maxlength="10" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label  class="fuente_lbl_controles">Describe containment inspection method in house/customer</label>
                                <input type="text" id="txt_estatus_descripcion" class="form-control input-sm" placeholder="Description" data-parsley-required="true" maxlength="250" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Corrective actions initiated?</label>
                                <select id="cmb_estatus_condicion_1" class="form-control input-sm" data-parsley-required="true">
                                    <option value="">--SELECT--</option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Customer afectation?</label>
                                <select id="cmb_estatus_condicion_2" class="form-control input-sm" data-parsley-required="true">
                                    <option value="">--SELECT--</option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label  class="fuente_lbl_controles">Other AAM plant afected?</label>
                                <select id="cmb_estatus_condicion_3" class="form-control input-sm" data-parsley-required="true">
                                    <option value="">--SELECT--</option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                </select>
                            </div>
                        </div>
                    &nbsp;&nbsp;
                    </div>
                    <%-- TABS & TABLES--%>
                 <div class="page-header">
                        &nbsp;
                    </div>
                  
                <div class="row" id="div_comentarios">
                        <div class="col-md-12 col-xs-12">
                            <label  class="fuente_lbl_controles">Daily status up date</label>
                            <textarea  id="txt_comentarios" class="form-control input-sm" rows="5" placeholder="Daily status up date" data-parsley-required="true"  style="min-height: 50px !important;"></textarea>
                                                        
                        </div>
                        <%-- <div class="col-md-2 col-xs-2">
                            <button id="btn_agregar_comentario" type="button" class="btn btn-info btn-sm" title="Agregar Comentario" style="margin:20px; padding: 4px 6px;" >
							    <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
						    </button>
                        </div>--%>  
                     <div class="page-header">
                        &nbsp;
                    </div>
				    <table id="tbl_lista_comentarios" class="table table-responsive"></table>                   
                    </div>                                                                       
                   

                </div>
            </div>
        </div>
</asp:Content>


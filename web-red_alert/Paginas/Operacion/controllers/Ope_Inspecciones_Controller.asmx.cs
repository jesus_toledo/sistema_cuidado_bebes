﻿using datos_red_alert;
using LitJson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;
using System.Drawing;
using System.Windows;

namespace web_red_alert.Paginas.Operacion.controllers
{
    /// <summary>
    /// Summary description for Ope_Inspecciones_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Inspecciones_Controller : System.Web.Services.WebService
    {

        /// <summary>
        /// Método para consultar los datos de una inspección
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Ver_Inspeccion(string jsonObject)
        {
            Cls_Ope_Inspecciones_Negocio obj_inspeccion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Inspecciones_Negocio> Lista_Inspecciones = new List<Cls_Ope_Inspecciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Object> lst = new List<Object>();

            try
            {
                obj_inspeccion= JsonMapper.ToObject<Cls_Ope_Inspecciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Inspeccion = (from _inspeccion in dbContext.Ope_Inspecciones                                   
                                       where (((obj_inspeccion.Inspeccion_ID != 0) ? _inspeccion.Inspeccion_ID.Equals(obj_inspeccion.Inspeccion_ID) : true) &&
                                           (!string.IsNullOrEmpty(obj_inspeccion.Codigo_Barras) ? _inspeccion.Codigo_Barras.ToLower().Contains(obj_inspeccion.Codigo_Barras.ToLower()) : true) &&
                                           (!string.IsNullOrEmpty(obj_inspeccion.Dispositivo_ID) ? _inspeccion.Dispositivo_ID.ToLower().Contains(obj_inspeccion.Dispositivo_ID.ToLower()) : true))


                                      select new Cls_Ope_Inspecciones_Negocio
                                       {                                          
                                           Inspeccion_ID = _inspeccion.Inspeccion_ID,
                                           Dispositivo_ID = _inspeccion.Dispositivo_ID,
                                           Dispositivo = _inspeccion.Dispositivo,
                                           Latitud = _inspeccion.Latitud,
                                           Longitud = _inspeccion.Longitud,
                                           Nivel_Bateria = _inspeccion.Nivel_Bateria,
                                           Codigo_Barras = _inspeccion.Codigo_Barras,                                       
                                           Fecha_Creo = _inspeccion.Fecha_Creo

                                       }).GroupBy(x => new
                                       {
                                           x.Inspeccion_ID,
                                           x.Dispositivo_ID,
                                           x.Dispositivo,
                                           x.Latitud,
                                           x.Longitud,
                                           x.Nivel_Bateria,
                                           x.Codigo_Barras,
                                           x.Fecha_Creo

                                       }, (key, group) => new Cls_Ope_Inspecciones_Negocio
                                       {                                          
                                           Inspeccion_ID = key.Inspeccion_ID,
                                           Dispositivo_ID = key.Dispositivo_ID,
                                           Dispositivo = key.Dispositivo,
                                           Latitud = key.Latitud,
                                           Longitud = key.Longitud,
                                           Nivel_Bateria = key.Nivel_Bateria,
                                           Codigo_Barras = key.Codigo_Barras,
                                           Fecha_Creo = key.Fecha_Creo

                                       }).OrderByDescending(u => u.Inspeccion_ID);

                    foreach (var p in Inspeccion)
                        Lista_Inspecciones.Add((Cls_Ope_Inspecciones_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Inspecciones);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método para consultar las inspecciones
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Inspecciones(string jsonObject)
        {
            Cls_Ope_Inspecciones_Negocio Obj_Inspeccion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Inspecciones_Negocio> Lista_inspecciones = new List<Cls_Ope_Inspecciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Inspeccion = JsonMapper.ToObject<Cls_Ope_Inspecciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Inspecciones = (from _inspecciones in dbContext.Ope_Inspecciones                                                                   
                                   where (Obj_Inspeccion.Inspeccion_ID != 0 ? _inspecciones.Inspeccion_ID.Equals(Obj_Inspeccion.Inspeccion_ID) : true) 

                                   select new Cls_Ope_Inspecciones_Negocio
                                   {
                                       Inspeccion_ID = _inspecciones.Inspeccion_ID,
                                       Dispositivo_ID = _inspecciones.Dispositivo_ID,
                                       Dispositivo = _inspecciones.Dispositivo,
                                       Latitud = _inspecciones.Latitud,
                                       Longitud = _inspecciones.Longitud,
                                       Nivel_Bateria = _inspecciones.Nivel_Bateria,
                                       Codigo_Barras = _inspecciones.Codigo_Barras,                                   
                                       Fecha_Creo = _inspecciones.Fecha_Creo                                   

                                   }).OrderByDescending(u => u.Inspeccion_ID);

                    foreach (var p in Inspecciones)
                        Lista_inspecciones.Add((Cls_Ope_Inspecciones_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_inspecciones);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar una inspección
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Inspeccion(string jsonObject)
        {
            Cls_Ope_Inspecciones_Negocio Obj_Inspeccion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Inspecciones_Negocio> Lista_inspecciones = new List<Cls_Ope_Inspecciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Inspeccion = JsonMapper.ToObject<Cls_Ope_Inspecciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {                   
                    var Inspecciones = (from _inspecciones in dbContext.Ope_Inspecciones
                                   where 
                                   _inspecciones.Inspeccion_ID.Equals(Obj_Inspeccion.Inspeccion_ID)

                                   select new Cls_Ope_Inspecciones_Negocio
                                   {
                                       Inspeccion_ID = _inspecciones.Inspeccion_ID,
                                       Dispositivo_ID = _inspecciones.Dispositivo_ID,
                                       Dispositivo = _inspecciones.Dispositivo,
                                       Latitud = _inspecciones.Latitud,
                                       Longitud = _inspecciones.Longitud,
                                       Nivel_Bateria = _inspecciones.Nivel_Bateria,
                                       Codigo_Barras = _inspecciones.Codigo_Barras,
                                       Fecha_Creo = _inspecciones.Fecha_Creo

                                   }).OrderByDescending(u => u.Inspeccion_ID);
                    foreach (var p in Inspecciones)
                        Lista_inspecciones.Add((Cls_Ope_Inspecciones_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_inspecciones);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar las acciones de cada inspección
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Acciones(string jsonObject)
        {
            Cls_Cat_Acciones_Negocio Obj_Accion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Acciones_Negocio> Lista_Detalles = new List<Cls_Cat_Acciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Accion = JsonMapper.ToObject<Cls_Cat_Acciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Cat_Acciones
                                       where _detalle.Inspeccion_ID.Equals(Obj_Accion.Inspeccion_ID)

                                       select new Cls_Cat_Acciones_Negocio
                                       {
                                           Accion_ID = _detalle.Accion_ID,
                                           Accion = _detalle.Accion,
                                           //PSI = _detalle.PSI,
                                           Tipo = _detalle.Tipo,
                                           Fecha = _detalle.Fecha,
                                           Estatus = _detalle.Estatus,
                                           Observaciones = _detalle.Observaciones
                                       }).OrderBy(u => u.Accion_ID);
                                       //}).OrderByDescending(u => u.Accion_ID);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método para consultar la longitud y latitud
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ubicacion_Inspeccion(string jsonObject)
        {
            Cls_Ope_Inspecciones_Negocio Obj_Accion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Inspecciones_Negocio> Lista_Detalles = new List<Cls_Ope_Inspecciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Accion = JsonMapper.ToObject<Cls_Ope_Inspecciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Inspecciones
                                       where _detalle.Inspeccion_ID.Equals(Obj_Accion.Inspeccion_ID)

                                       select new Cls_Ope_Inspecciones_Negocio
                                       { 
                                           Inspeccion_ID = _detalle.Inspeccion_ID,
                                           Latitud = _detalle.Latitud,
                                           Longitud = _detalle.Longitud
                                           
                                       }).OrderByDescending(u => u.Inspeccion_ID);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

    }
}

﻿using datos_red_alert;
using LitJson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Paginas_Generales.Operacion.controllers
{
    /// <summary>
    /// Summary description for Ope_Reproducciones
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Reproducciones : System.Web.Services.WebService
    {
        /// <summary>
        /// Método para consultar los datos de una inspección
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Ver_Reproduccion(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio obj_Reproduccion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Reproduccion = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Object> lst = new List<Object>();

            try
            {
                obj_Reproduccion = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Reproduccion = (from _Reproduccion in dbContext.Ope_Reproducciones
                                        where (((obj_Reproduccion.Reproduccion_ID != 0) ? _Reproduccion.Reproduccion_ID.Equals(obj_Reproduccion.Reproduccion_ID) : true) &&
                                            (!string.IsNullOrEmpty(obj_Reproduccion.Video) ? _Reproduccion.Video.ToLower().Contains(obj_Reproduccion.Video.ToLower()) : true) &&
                                            (!string.IsNullOrEmpty(obj_Reproduccion.folio_reproduccion) ? _Reproduccion.folio_reproduccion.ToLower().Contains(obj_Reproduccion.folio_reproduccion.ToLower()) : true))


                                        select new Cls_Ope_Reproducciones_Negocio
                                        {
                                            Reproduccion_ID = _Reproduccion.Reproduccion_ID,
                                            Video = _Reproduccion.Video,
                                            folio_reproduccion = _Reproduccion.folio_reproduccion,
                                            Latitud = _Reproduccion.Latitud,
                                            Longitud = _Reproduccion.Longitud,
                                            Fecha_Creo = _Reproduccion.Fecha_Creo

                                        }).GroupBy(x => new
                                        {
                                            x.Reproduccion_ID,
                                            x.Video,
                                            x.folio_reproduccion,
                                            x.Latitud,
                                            x.Longitud,
                                            x.Fecha_Creo

                                        }, (key, group) => new Cls_Ope_Reproducciones_Negocio
                                        {
                                            Reproduccion_ID = key.Reproduccion_ID,
                                            Video = key.Video,
                                            folio_reproduccion = key.folio_reproduccion,
                                            Latitud = key.Latitud,
                                            Longitud = key.Longitud,
                                            Fecha_Creo = key.Fecha_Creo

                                        }).OrderByDescending(u => u.Reproduccion_ID);

                    // var result = myList.GroupBy(test => test.id)
                    //.Select(grp => grp.First())
                    //.ToList();

                    foreach (var p in Reproduccion)
                        Lista_Reproduccion.Add((Cls_Ope_Reproducciones_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Reproduccion);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método para consultar las inspecciones
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Reproducciones(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio Obj_Reproducciones = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Reproducciones = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Reproducciones = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);


                    var Reproducciones = (from _Reproducciones in dbContext.Ope_Reproducciones
                                          where (!string.IsNullOrEmpty(Obj_Reproducciones.folio_reproduccion) ? _Reproducciones.folio_reproduccion.ToLower().Contains(Obj_Reproducciones.folio_reproduccion.ToLower()) : true)
                                          select new Cls_Ope_Reproducciones_Negocio
                                          {
                                              Reproduccion_ID = _Reproducciones.Reproduccion_ID,
                                              Video = _Reproducciones.Video,
                                              folio_reproduccion = _Reproducciones.folio_reproduccion,
                                              Latitud = _Reproducciones.Latitud,
                                              Longitud = _Reproducciones.Longitud,
                                              Fecha_Creo = _Reproducciones.Fecha_Creo
                                              // }).OrderBy(u => u.folio_reproduccion);
                                          }).GroupBy(g => new
                                          {
                                              g.folio_reproduccion
                                             
                                          }).Select(s => s.FirstOrDefault());

                    foreach (var p in Reproducciones)
                        Lista_Reproducciones.Add((Cls_Ope_Reproducciones_Negocio)p);
                  

                    Json_Resultado = JsonMapper.ToJson(Lista_Reproducciones);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar una inspección
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Reproduccion(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio Obj_Reproduccion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Reproduccion = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Reproduccion = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Reproduccion = (from _Reproduccion in dbContext.Ope_Reproducciones
                                        where
                                        _Reproduccion.folio_reproduccion.Equals(Obj_Reproduccion.folio_reproduccion)

                                        select new Cls_Ope_Reproducciones_Negocio
                                        {
                                            Reproduccion_ID = _Reproduccion.Reproduccion_ID,
                                            Video = _Reproduccion.Video,
                                            folio_reproduccion = _Reproduccion.folio_reproduccion,
                                            Latitud = _Reproduccion.Latitud,
                                            Longitud = _Reproduccion.Longitud,
                                            Fecha_Creo = _Reproduccion.Fecha_Creo
                                      
                                        }).OrderByDescending(u => u.folio_reproduccion.FirstOrDefault());
                    foreach (var p in Reproduccion)
                        Lista_Reproduccion.Add((Cls_Ope_Reproducciones_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Reproduccion);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar las acciones de cada inspección
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Detalles(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio Obj_Accion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Detalles = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Accion = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Reproducciones
                                       where _detalle.folio_reproduccion.Equals(Obj_Accion.folio_reproduccion)

                                       select new Cls_Ope_Reproducciones_Negocio
                                       {
                                           Video = _detalle.Video,
                                           Latitud = _detalle.Latitud,
                                           Longitud = _detalle.Longitud,
                                           Fecha_Creo = _detalle.Fecha_Creo,
                                           folio_reproduccion = _detalle.folio_reproduccion
                                       }).OrderBy(u => u.folio_reproduccion);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        /// <summary>
        /// Método para consultar la longitud y latitud
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ubicacion_Inspeccion(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio Obj_Accion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Detalles = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Accion = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Reproducciones
                                       where _detalle.folio_reproduccion.Equals(Obj_Accion.folio_reproduccion)

                                       select new Cls_Ope_Reproducciones_Negocio
                                       {
                                           folio_reproduccion = _detalle.folio_reproduccion,
                                           Reproduccion_ID = _detalle.Reproduccion_ID,
                                           Latitud = _detalle.Latitud,
                                           Longitud = _detalle.Longitud

                                       }).OrderByDescending(u => u.folio_reproduccion);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar la longitud y latitud
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Ubicacion(string jsonObject)
        {
            Cls_Ope_Reproducciones_Negocio Obj_Accion = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Reproducciones_Negocio> Lista_Detalles = new List<Cls_Ope_Reproducciones_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Accion = JsonMapper.ToObject<Cls_Ope_Reproducciones_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Reproducciones
                                       where _detalle.folio_reproduccion.Equals(Obj_Accion.folio_reproduccion)

                                       select new Cls_Ope_Reproducciones_Negocio
                                       {
                                           folio_reproduccion = _detalle.folio_reproduccion,
                                           Reproduccion_ID = _detalle.Reproduccion_ID,
                                           Latitud = _detalle.Latitud,
                                           Longitud = _detalle.Longitud
                                           //}).OrderByDescending(u => u.folio_reproduccion);
                                       }).GroupBy(g => new
                                       {
                                           g.folio_reproduccion

                                       }).Select(s => s.FirstOrDefault());


                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


    }
}

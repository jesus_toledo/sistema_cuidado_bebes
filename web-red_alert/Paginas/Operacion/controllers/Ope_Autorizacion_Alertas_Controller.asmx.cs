﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Web.Services;

namespace web_red_alert.Paginas.Operacion.controllers
{
    /// <summary>
    /// Descripción breve de Ope_Autorizacion_Alertas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Autorizacion_Alertas_Controller : System.Web.Services.WebService
    {
        #region Métodos
        /// <summary>
        /// Método para actualizar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Detalles_Negocio> lst_Criterios = new List<Cls_Ope_Alertas_Rojas_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update Alert";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);
                
                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    _alerta.Estatus_Partes_1 = Obj_Alerta_Roja.Estatus_Partes_1;
                    _alerta.Estatus_Partes_2 = Obj_Alerta_Roja.Estatus_Partes_2;
                    _alerta.Estatus_Partes_3 = Obj_Alerta_Roja.Estatus_Partes_3;
                    _alerta.Estatus_Partes_4 = Obj_Alerta_Roja.Estatus_Partes_4;
                    _alerta.Estatus_Descripcion = Obj_Alerta_Roja.Estatus_Descripcion;
                    _alerta.Estatus_Condicion_1 = Obj_Alerta_Roja.Estatus_Condicion_1;
                    _alerta.Estatus_Condicion_2 = Obj_Alerta_Roja.Estatus_Condicion_2;
                    _alerta.Estatus_Condicion_3 = Obj_Alerta_Roja.Estatus_Condicion_3;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now).Value;
                    
                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las alertas rojas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Alertas_Rojas(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_alertas = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var _responsable_actual = dbContext.Cat_Responsables_Autorizacion.Where(x => x.No_Empleado == Cls_Sesiones.Datos_Empleados.No_Empleado).First();
                    //var _criterio_actual = dbContext.Cat_Criterios_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == _responsable_actual.Criterio_Autorizacion_ID).First();

                    var _hijos = (from a in dbContext.Cat_Criterios_Autorizacion where a.Criterio_Autorizacion_ID == _responsable_actual.Criterio_Autorizacion_ID select a.Criterio_Autorizacion_ID);
                    var _responsables = (from b in dbContext.Cat_Responsables_Autorizacion where _hijos.Contains(b.Criterio_Autorizacion_ID) select b.No_Empleado);
                    var _no_alertas_rojas = (from c in dbContext.Ope_Responsables_Autorizacion where _responsables.Contains(c.No_Empleado) select c.No_Alerta_Roja);

                    var Alertas = (from _alertas in dbContext.Ope_Alertas_Rojas
                                   join _estatus in dbContext.Apl_Estatus on _alertas.Estatus_ID equals _estatus.Estatus_ID
                                   join _plantas in dbContext.Apl_Plantas on new { _alertas.Planta_ID, _alertas.Empresa_ID } equals new { _plantas.Planta_ID, _plantas.Empresa_ID }
                                   join _unidad_negocio in dbContext.Cat_Unidades_Negocio on _alertas.Unidad_Negocio_ID equals _unidad_negocio.Unidad_Negocio_ID
                                   join _ope_responsable in dbContext.Ope_Responsables_Autorizacion on _alertas.No_Alerta_Roja equals _ope_responsable.No_Alerta_Roja
                                   from _numero_parte in dbContext.Cat_Numero_Partes.Where(x => x.Numero_Parte_ID == _alertas.Numero_Parte_ID).DefaultIfEmpty()

                                   where _alertas.Empresa_ID.Equals(empresa_id) &&
                                   _estatus.Estatus != "DELETED" &&
                                   _no_alertas_rojas.Contains(_alertas.No_Alerta_Roja) &&
                                   _ope_responsable.No_Empleado == Cls_Sesiones.Datos_Empleados.No_Empleado

                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alertas.No_Alerta_Roja,
                                       Empresa_ID = _alertas.Empresa_ID,
                                       Estatus = _estatus.Estatus,
                                       Estatus_ID = _alertas.Estatus_ID,
                                       Planta = _plantas.Nombre,
                                       Planta_ID = _alertas.Planta_ID,
                                       Unidad_Negocio = _unidad_negocio.Nombre,
                                       Unidad_Negocio_ID = _alertas.Unidad_Negocio_ID,
                                       Producto_ID = _alertas.Producto_ID,
                                       Numero_Parte_ID = _alertas.Numero_Parte_ID,
                                       Numero_Parte = _alertas.Numero_Parte != null ? _alertas.Numero_Parte : _numero_parte.Nombre,
                                       Cliente_ID = _alertas.Cliente_ID,
                                       Referencia_Producto = _alertas.Referencia_Producto,
                                       Sitio_Cliente = _alertas.Sitio_Cliente,
                                       Vehiculo = _alertas.Vehiculo,
                                       Area_ID = _alertas.Area_ID,
                                       Turno_ID = _alertas.Turno_ID,
                                       Aprobado = _ope_responsable.Aprobado,
                                       No_Empleado = _alertas.No_Empleado

                                   }).OrderByDescending(u => u.No_Alerta_Roja);

                    foreach (var p in Alertas)
                        Lista_alertas.Add((Models.Negocio.Cls_Ope_Alertas_Rojas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar una alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_alertas = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    string n_producto = Obj_Alerta_Roja.Producto_ID == null ? "" :
                        dbContext.Cat_Productos.Where(u => u.Producto_ID == Obj_Alerta_Roja.Producto_ID).Select(u => u.Nombre).First();
                    string n_cliente = Obj_Alerta_Roja.Cliente_ID == null ? "" :
                        dbContext.Cat_Clientes.Where(u => u.Cliente_ID == Obj_Alerta_Roja.Cliente_ID).Select(u => u.Nombre).First();
                    string n_area = Obj_Alerta_Roja.Area_ID == 0 ? "" :
                        dbContext.Cat_Areas.Where(u => u.Area_ID == Obj_Alerta_Roja.Area_ID).Select(u => u.Nombre).First();
                    string n_turno = Obj_Alerta_Roja.Turno_ID == 0 ? "" :
                        dbContext.Cat_Turnos.Where(u => u.Turno_ID == Obj_Alerta_Roja.Turno_ID).Select(u => u.Nombre).First();
                    string n_numero_parte = Obj_Alerta_Roja.Numero_Parte_ID == null ? "" :
                        dbContext.Cat_Numero_Partes.Where(u => u.Numero_Parte_ID == Obj_Alerta_Roja.Numero_Parte_ID).Select(u => u.Nombre).First();
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _alertas in dbContext.Ope_Alertas_Rojas
                                   where _alertas.Empresa_ID.Equals(empresa_id) &&
                                   _alertas.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)

                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alertas.No_Alerta_Roja,
                                       Producto = n_producto,
                                       Numero_Parte_ID = _alertas.Numero_Parte_ID,
                                       Numero_Parte = n_numero_parte,
                                       Cliente = n_cliente,
                                       Referencia_Producto = _alertas.Referencia_Producto,
                                       Sitio_Cliente = _alertas.Sitio_Cliente,
                                       Vehiculo = _alertas.Vehiculo,
                                       Area = n_area,
                                       Turno = n_turno,
                                       Numero_CAR = _alertas.Numero_CAR,
                                       Descripcion_1 = _alertas.Descripcion_1,
                                       Descripcion_2 = _alertas.Descripcion_2,
                                       Descripcion_3 = _alertas.Descripcion_3,
                                       Descripcion_4 = _alertas.Descripcion_4,
                                       Descripcion_5 = _alertas.Descripcion_5,
                                       Descripcion_6 = _alertas.Descripcion_6,
                                       Descripcion_7 = _alertas.Descripcion_7,
                                       Condicion_Buena = _alertas.Condicion_Buena,
                                       Condicion_Mala = _alertas.Condicion_Mala,
                                       Estatus_Partes_1 = _alertas.Estatus_Partes_1,
                                       Estatus_Partes_2 = _alertas.Estatus_Partes_2,
                                       Estatus_Partes_3 = _alertas.Estatus_Partes_3,
                                       Estatus_Partes_4 = _alertas.Estatus_Partes_4,
                                       Estatus_Descripcion = _alertas.Estatus_Descripcion,
                                       Estatus_Condicion_1 = _alertas.Estatus_Condicion_1,
                                       Estatus_Condicion_2 = _alertas.Estatus_Condicion_2,
                                       Estatus_Condicion_3 = _alertas.Estatus_Condicion_3,
                                       Observaciones = _alertas.Observaciones


                                   }).OrderByDescending(u => u.No_Alerta_Roja);
                    foreach (var p in Alertas)
                        Lista_alertas.Add((Cls_Ope_Alertas_Rojas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para rechazar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Rechazar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _responsable_actual = dbContext.Cat_Responsables_Autorizacion.Where(r => r.No_Empleado == Cls_Sesiones.Datos_Empleados.No_Empleado).First();
                    var _crierio_actual = dbContext.Cat_Criterios_Autorizacion.Where(c => c.Criterio_Autorizacion_ID == _responsable_actual.Criterio_Autorizacion_ID).First();
                    var _estatus_cancelado = dbContext.Apl_Estatus.Where(e => e.Estatus == "CANCELED").First();
                    var _estatus_rechazado = dbContext.Apl_Estatus.Where(p => p.Estatus == "REJECTED").First();
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    if (_crierio_actual.Nombre.Trim() == "Nivel 3" && _crierio_actual.Tipo_Criterio.Trim() == "Quality")
                        _alerta.Estatus_ID = _estatus_cancelado.Estatus_ID;
                    else if (_crierio_actual.Nodo_ID == 0)
                        _alerta.Estatus_ID = _estatus_cancelado.Estatus_ID;
                    else
                        _alerta.Estatus_ID = _estatus_rechazado.Estatus_ID;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now);

                    //Actualizar Responsable_Autorizo
                    var _autorizo = dbContext.Ope_Responsables_Autorizacion.Where(x => x.No_Alerta_Roja == Obj_Alerta_Roja.No_Alerta_Roja && x.No_Empleado == _responsable_actual.No_Empleado).First();
                    _autorizo.Aprobado = "NO";

                    dbContext.SaveChanges();
                    var _iniciador_alerta = (from _responsables in dbContext.Cat_Responsables_Autorizacion
                                             where _responsables.No_Empleado == _alerta.No_Empleado
                                             select new Cls_Cat_Responsables_Autorizacion_Negocio
                                             {
                                                 Responsable_ID = _responsables.Responsable_ID,
                                                 Nombre = _responsables.Nombre,
                                                 Email = _responsables.Email,
                                                 No_Empleado = _responsables.No_Empleado,
                                                 Telefono = _responsables.Telefono
                                             }).OrderBy(u => u.Nombre).ToList();

                    Enviar_Correo_Rechazado(_iniciador_alerta, _alerta);

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para autorizar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Autorizar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Autoririzar  alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _responsable_actual = dbContext.Cat_Responsables_Autorizacion.Where(u => u.No_Empleado == Cls_Sesiones.Datos_Empleados.No_Empleado).First();
                    var _criterio_actual = dbContext.Cat_Criterios_Autorizacion.Where(x => x.Criterio_Autorizacion_ID == _responsable_actual.Criterio_Autorizacion_ID).First();
                    var _estatus_proceso = dbContext.Apl_Estatus.Where(p => p.Estatus == "PROCESS").First();
                    var _estatus_cerrado = dbContext.Apl_Estatus.Where(p => p.Estatus == "CLOSED").First();
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    if (_criterio_actual.Nombre.Trim() == "Nivel 3" && _criterio_actual.Tipo_Criterio.Trim() == "Quality")
                        _alerta.Estatus_ID = _estatus_cerrado.Estatus_ID;
                    else if (_criterio_actual.Nodo_ID == 0)
                        _alerta.Estatus_ID = _estatus_cerrado.Estatus_ID;
                    else
                        _alerta.Estatus_ID = _estatus_proceso.Estatus_ID;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now);

                    //Agregar responsables siguiente nivel

                    if (_criterio_actual.Nodo_ID > 0)
                    {
                        var _responsables_autorizacion = (from _responsables in dbContext.Cat_Responsables_Autorizacion
                                                          where _responsables.Criterio_Autorizacion_ID == _criterio_actual.Nodo_ID
                                                          select new Cls_Cat_Responsables_Autorizacion_Negocio
                                                          {
                                                              Responsable_ID = _responsables.Responsable_ID,
                                                              No_Empleado = _responsables.No_Empleado,
                                                              Nombre = _responsables.Nombre,
                                                              Email = _responsables.Email,
                                                              Telefono = _responsables.Telefono
                                                          }).OrderBy(u => u.No_Empleado);

                        foreach (var _responsable in _responsables_autorizacion)
                        {
                            var _existe_responsable = (from _responsables in dbContext.Ope_Responsables_Autorizacion
                                                       where _responsables.No_Empleado == _responsable.No_Empleado &&
                                                       _responsables.No_Alerta_Roja == Obj_Alerta_Roja.No_Alerta_Roja
                                                       select new { _responsables.No_Alerta_Roja, _responsables.No_Empleado });
                            if (_existe_responsable.Any())
                            {
                                //do nothing
                            }
                            else
                            {
                                Ope_Responsables_Autorizacion nuevo_responsable = new Ope_Responsables_Autorizacion();
                                nuevo_responsable.No_Alerta_Roja = Obj_Alerta_Roja.No_Alerta_Roja;
                                nuevo_responsable.No_Empleado = _responsable.No_Empleado;
                                nuevo_responsable.Aprobado = "PENDING";
                                nuevo_responsable.Usuario_Creo = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                                nuevo_responsable.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                                dbContext.Ope_Responsables_Autorizacion.Add(nuevo_responsable);
                            }
                        }
                        Enviar_Correo(_responsables_autorizacion.ToList(), _alerta);
                    }
                    else
                    {
                        var _autorizado = dbContext.Apl_Estatus.Where(p => p.Estatus == "CLOSED").First();
                        _alerta.Estatus_ID = _autorizado.Estatus_ID;
                    }

                    //Actualizar Responsable_Autorizo
                    var _autorizo = dbContext.Ope_Responsables_Autorizacion.Where(x => x.No_Alerta_Roja == Obj_Alerta_Roja.No_Alerta_Roja && x.No_Empleado == _responsable_actual.No_Empleado).First();
                    _autorizo.Aprobado = "YES";

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }

        /// Método para actualizar las observaciones de la alerta
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Observaciones_Alerta(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Rechazar alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    _alerta.Observaciones = Obj_Alerta_Roja.Observaciones;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los numeros de partes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Numero_Partes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Numero_Partes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_partes = (from _partes in dbContext.Cat_Numero_Partes
                                       where _partes.Nombre.Contains(q)
                                       select new Cls_Select2
                                       {
                                           id = _partes.Numero_Parte_ID.ToString(),
                                           text = _partes.Nombre,
                                           tag = String.Empty
                                       }).OrderBy(u => u.text);

                    if (_lst_partes.Any())
                        foreach (var p in _lst_partes)
                            Lista_Numero_Partes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Numero_Partes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
    
        /// <summary>
        /// Método para consultar los estatus
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Estatus = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_estatus = (from _estatus in dbContext.Apl_Estatus
                                        where _estatus.Estatus.Contains(q)
                                        select new Cls_Select2
                                        {
                                            id = _estatus.Estatus_ID.ToString(),
                                            text = _estatus.Estatus,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);
                    if (_lst_estatus.Any())
                        foreach (var p in _lst_estatus)
                            Lista_Estatus.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Estatus);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus_Activo()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  where _empresas.Estatus == "OPENED"
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };

                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las plantas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Plantas()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Plantas = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_plantas = (from _plantas in dbContext.Apl_Plantas
                                        where _plantas.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                        && _plantas.Nombre.Contains(q)
                                        select new Cls_Select2
                                        {
                                            id = _plantas.Planta_ID.ToString(),
                                            text = _plantas.Nombre,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);

                    if (_lst_plantas.Any())
                        foreach (var p in _lst_plantas)
                            Lista_Plantas.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Plantas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las unidades de negocio
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Unidades_Negocio()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Unidades = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_unidades = (from _unidades in dbContext.Cat_Unidades_Negocio
                                         where _unidades.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _unidades.Nombre.Contains(q)
                                         select new Cls_Select2
                                         {
                                             id = _unidades.Unidad_Negocio_ID.ToString(),
                                             text = _unidades.Nombre,
                                             tag = String.Empty
                                         }).OrderBy(u => u.text);

                    if (_lst_unidades.Any())
                        foreach (var p in _lst_unidades)
                            Lista_Unidades.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Unidades);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los productos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Productos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Productos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_productos = (from _productos in dbContext.Cat_Productos
                                          where _productos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                          && _productos.Nombre.Contains(q)
                                          select new Cls_Select2
                                          {
                                              id = _productos.Producto_ID.ToString(),
                                              text = _productos.Nombre,
                                              tag = String.Empty
                                          }).OrderBy(u => u.text);

                    if (_lst_productos.Any())
                        foreach (var p in _lst_productos)
                            Lista_Productos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Productos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los turnos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Turnos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Turnos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_turnos = (from _turnos in dbContext.Cat_Turnos
                                       where _turnos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                       && _turnos.Nombre.Contains(q)
                                       select new Cls_Select2
                                       {
                                           id = _turnos.Turno_ID.ToString(),
                                           text = _turnos.Nombre,
                                           tag = String.Empty
                                       }).OrderBy(u => u.text);

                    if (_lst_turnos.Any())
                        foreach (var p in _lst_turnos)
                            Lista_Turnos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Turnos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las areas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Areas()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Areas = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_areas = (from _areas in dbContext.Cat_Areas
                                      where _areas.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                      && _areas.Nombre.Contains(q)
                                      select new Cls_Select2
                                      {
                                          id = _areas.Area_ID.ToString(),
                                          text = _areas.Nombre,
                                          tag = String.Empty
                                      }).OrderBy(u => u.text);

                    if (_lst_areas.Any())
                        foreach (var p in _lst_areas)
                            Lista_Areas.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Areas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los clientes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Clientes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Clientes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_clientes = (from _clientes in dbContext.Cat_Clientes
                                         where _clientes.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _clientes.Nombre.Contains(q)
                                         select new Cls_Select2
                                         {
                                             id = _clientes.Cliente_ID.ToString(),
                                             text = _clientes.Nombre,
                                             tag = String.Empty
                                         }).OrderBy(u => u.text);

                    if (_lst_clientes.Any())
                        foreach (var p in _lst_clientes)
                            Lista_Clientes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las plantas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Unidades_Negocio_Filtro(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = new Cls_Cat_Unidades_Negocio_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Unidades_Negocio_Negocio> Lista_Unidades = new List<Cls_Cat_Unidades_Negocio_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _lst_unidades = (from _unidad_negocio in dbContext.Cat_Unidades_Negocio
                                         join _estatus in dbContext.Apl_Estatus on _unidad_negocio.Estatus_ID equals _estatus.Estatus_ID
                                         where _unidad_negocio.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _unidad_negocio.Planta_ID == Obj_Unidades_Negocio.Planta_ID
                                         && _estatus.Estatus == "ACTIVE"

                                         select new Cls_Cat_Unidades_Negocio_Negocio
                                         {
                                             Unidad_Negocio_ID = _unidad_negocio.Unidad_Negocio_ID,
                                             Planta_ID = _unidad_negocio.Planta_ID,
                                             Nombre = _unidad_negocio.Nombre
                                         }).OrderBy(u => u.Unidad_Negocio_ID);

                    foreach (var p in _lst_unidades)
                        Lista_Unidades.Add((Cls_Cat_Unidades_Negocio_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Unidades);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Criterios(string jsonObject)
        {
            Cls_Cat_Criterios_Negocio Obj_Criterios = null;
            string Json_Resultado = string.Empty;
            List<Cat_Criterios> Lista_criterios = new List<Cat_Criterios>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Negocio>(jsonObject);
                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    //var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID.ToString() == Cls_Sesiones.Empleado_ID).First();
                    var _responsable = dbContext.Cat_Responsables_Autorizacion.Where(u => u.Nombre == Cls_Sesiones.Empleado).First();
                    var _criterio_autorizacion = dbContext.Cat_Criterios_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == _responsable.Criterio_Autorizacion_ID).First();

                    var Criterios = from _criterios in dbContext.Cat_Criterios
                                    where _criterios.Descripcion.ToLower().Contains(_criterio_autorizacion.Tipo_Criterio.ToLower())
                                    select new { _criterios.Nombre, _criterios.Criterio_ID };

                    Json_Resultado = JsonMapper.ToJson(Criterios.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los gerentes de una alerta
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Alertas_Criterios(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Detalles_Negocio obj_Alerta = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Alertas_Rojas_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_Alerta = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Detalles_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Alertas_Rojas_Detalles

                                       where _detalle.No_Alerta_Roja.Equals(obj_Alerta.No_Alerta_Roja)

                                       select new Cls_Ope_Alertas_Rojas_Detalles_Negocio
                                       {
                                           Criterio_ID = _detalle.Criterio_ID,
                                           Valor = _detalle.Valor,
                                           Observaciones = _detalle.Observaciones

                                       }).OrderByDescending(u => u.Criterio_ID);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// Método para consultar los procesos criterios de una alerta
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Alertas_Procesos(string jsonObject)
        {
            Cls_Ope_Alertas_Procesos_Detalles_Negocio obj_Alerta = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Procesos_Detalles_Negocio> Lista_Detalles = new List<Cls_Ope_Alertas_Procesos_Detalles_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_Alerta = JsonMapper.ToObject<Cls_Ope_Alertas_Procesos_Detalles_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var lst_detalle = (from _detalle in dbContext.Ope_Alertas_Procesos_Detalles
                                       join _procesos in dbContext.Cat_Procesos_Criterios on _detalle.Proceso_Criterio_ID equals _procesos.Proceso_Criterio_ID

                                       where _detalle.No_Alerta_Roja.Equals(obj_Alerta.No_Alerta_Roja)

                                       select new Cls_Ope_Alertas_Procesos_Detalles_Negocio
                                       {
                                           Proceso_Criterio_ID = _detalle.Proceso_Criterio_ID,
                                           Proceso_Criterio = _procesos.Nombre,
                                           Valor = _detalle.Valor,
                                           Observaciones = _detalle.Observaciones

                                       }).OrderByDescending(u => u.Proceso_Criterio);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar si la alerta tiene datos en estatus
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Verificar_Alerta(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio obj_Alerta = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_usuarios = new List<Cls_Select2>();

            try
            {
                obj_Alerta = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);
                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var _Alerta = (from _alertas in dbContext.Ope_Alertas_Rojas
                                    where _alertas.No_Alerta_Roja == obj_Alerta.No_Alerta_Roja &&
                                    _alertas.Estatus_Partes_1 != null

                                    select new Cls_Select2
                                    {
                                        text = _alertas.No_Alerta_Roja.ToString()
                                    }).OrderBy(u => u.text);

                    if(_Alerta.Any())
                    {
                        Mensaje.Mensaje = (true).ToString();
                    }
                    else
                    {
                        Mensaje.Mensaje = (false).ToString();
                    }
                    
                    //Mensaje.Mensaje = _Usuario.ToString();
                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }

            return Json_Resultado;
        }
        public string Enviar_Correo(List<Cls_Cat_Responsables_Autorizacion_Negocio> Lst_Responsables, Ope_Alertas_Rojas Alerta)
        {
            string Json_Resultado = String.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            //String[] archivos = new String[lst_Archivos.Count()];
            string ruta_archivo = string.Empty;
            //string ruta_logo = string.Empty;
            bool Enviado = false;
            string _asunto = "";
            string _texto = "";
            string Nombre_Planta = "";
            string Unidad_Negocios = "";
            string Nombre_Producto = "";
            string Nombre_Cliente = "";
            string Nombre_Area = "";
            string Nombre_Turno = "";
            string Nombre_Estatus = "";
            string strDominio_logo = System.Configuration.ConfigurationManager.AppSettings["http_ruta_logo"];
            string strDominio_Imagenes = System.Configuration.ConfigurationManager.AppSettings["http_ruta_imagenes"];
            string strDominio_Imagenes_buenas = System.Configuration.ConfigurationManager.AppSettings["http_ruta_imagen_buena"];
            string strDominio_Login = System.Configuration.ConfigurationManager.AppSettings["http_ruta_login"];

            string _rutaHtpplogo = "";
            _rutaHtpplogo = strDominio_logo + "logo_AAM.png";

            string _rutaHttpcondicion_buena = "";
            string nombre_condicion_buena = Alerta.Condicion_Buena;
            string[] nombre = nombre_condicion_buena.Split('/');
            _rutaHttpcondicion_buena = strDominio_Imagenes_buenas + nombre.GetValue(nombre.Length - 1);

            string _rutaHttpcondicion_mala = "";
            string nombre_condicion_mala = Alerta.Condicion_Mala;
            string[] nombres = nombre_condicion_mala.Split('/');
            _rutaHttpcondicion_mala = strDominio_Imagenes + nombres.GetValue(nombres.Length - 1);


            try
            {
                Cls_Base64_Image Base64_Image = new Cls_Base64_Image();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var Plantas = dbContext.Apl_Plantas.Where(e => e.Planta_ID == Alerta.Planta_ID).First();
                    Nombre_Planta = Plantas.Nombre;
                    var Unidades_Negocios = dbContext.Cat_Unidades_Negocio.Where(e => e.Unidad_Negocio_ID == Alerta.Unidad_Negocio_ID).First();
                    Unidad_Negocios = Unidades_Negocios.Nombre;
                    var Productos = dbContext.Cat_Productos.Where(e => e.Producto_ID == Alerta.Producto_ID).First();
                    Nombre_Producto = Productos.Nombre;
                    var Clientes = dbContext.Cat_Clientes.Where(e => e.Cliente_ID == Alerta.Cliente_ID).First();
                    Nombre_Cliente = Clientes.Nombre;
                    var Areas = dbContext.Cat_Areas.Where(e => e.Area_ID == Alerta.Area_ID).First();
                    Nombre_Area = Areas.Nombre;
                    var Turnos = dbContext.Cat_Turnos.Where(e => e.Turno_ID == Alerta.Turno_ID).First();
                    Nombre_Turno = Turnos.Nombre;
                    var Estatus = dbContext.Apl_Estatus.Where(e => e.Estatus_ID == Alerta.Estatus_ID).First();
                    Nombre_Estatus = Estatus.Estatus;
                }
                //foreach (var Alerta in Lst_Alertas)
                {
                    Mensaje.Titulo = "Send Red Alert " + Nombre_Estatus;
                    foreach (var Correo in Lst_Responsables)
                    {
                        //for (int _archivos = 0; _archivos < lst_Archivos.Count(); _archivos++)
                        //{
                        //    ruta_archivo = Server.MapPath("../" + lst_Archivos[_archivos].URL_Archivo);

                        //    if (File.Exists(ruta_archivo))
                        //    {
                        //        archivos[_archivos] = ruta_archivo;
                        //    }
                        //    else
                        //    {
                        //        Mensaje.Estatus = "error";
                        //        Mensaje.Mensaje = "<i class='fa fa-check' style='color: #00A41E;'></i>&nbsp;La orden cambio de proceso no se pudo enviar. No se encontró el archivo";
                        //    }
                        //}

                        _asunto = "Red Alert #" + Alerta.No_Alerta_Roja + " " + Nombre_Estatus;

                        //Estructura correo
                        //ruta_logo = Server.MapPath("../../../Recursos/img/logo_AAM.png");

                        _texto = "";
                        _texto += "<table>";
                        _texto += "   <tr>";
                        _texto += "         <td>";
                        _texto += "                   <img src ='" + _rutaHtpplogo+ "'alt = 'Imagen Buena' />";
                        _texto += "         </td>";
                        _texto += "         <td>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td>";
                        _texto += "         </td>";
                        _texto += "         <td align='right'>";
                        _texto += "                   <h3>Red Alert</h3>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <table>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>Please be informed about the following Quality / Launching concern:</td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>";
                        _texto += "                                </td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>You need to log in to the <a href='"+ strDominio_Login +"'>Red Alert System</a> to release the communication to the following levels by approving it.</td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td><p style='color:red;font-weight:bold'>Consider Red Alert " + Nombre_Estatus + " will not be released unless you approve it.</p></td>";
                        _texto += "                      </tr>";
                        _texto += "                 </table>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                                                               <table>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'  bgcolor='silver'><strong>GENERAL DATA</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>AAM Plant:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Planta + "</td>";
                        _texto += "                                                                                              <td>Date of information:</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Bussines Unit:</td>";
                        _texto += "                                                                                              <td>" + Unidad_Negocios + "</td>";
                        _texto += "                                                                                              <td>Date of update:</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Product Name:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Producto + "</td>";
                        _texto += "                                                                                              <td>1. Safety major issue</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>AAM part number:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Numero_Parte + "</td>";
                        _texto += "                                                                                              <td>2. Major project risk (Project issue that affect the SORP)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Cliente + "</td>";
                        _texto += "                                                                                              <td>3. Major risks/incidents (Recall, Soft recall, 0km, Rework)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer product reference:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Referencia_Producto + "</td>";
                        _texto += "                                                                                              <td>4. Major issue found at Critical Safety Operations (17 Processes)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer site(s):</td>";
                        _texto += "                                                                                              <td>" + Alerta.Sitio_Cliente + "</td>";
                        _texto += "                                                                                              <td>5. Metallic/metalurgycal failure</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Product Model:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Vehiculo + "</td>";
                        _texto += "                                                                                              <td>6. Customer/IATF certification lost</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Afected Process/Area:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Area + "</td>";
                        _texto += "                                                                                              <td>7. Major warranty concerns (generate recall or soft recall)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Related shift:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Turno + "</td>";
                        _texto += "                                                                                              <td>8. Major internal concerns (scrap, machine breakdown, ..)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>CAR#:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Numero_CAR + "</td>";
                        _texto += "                                                                                              <td>9. Supplier quality/delivery issue</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'  bgcolor='silver'><strong>PROBLEM DESCRIPTION (5W+2H)</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Refuse Description</td>";
                        _texto += "                                                                                              <td colspan='3' style='color:red'>" + Alerta.Observaciones + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>What happened?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_1 + "</td>";
                        _texto += "                                                                                              <td bgcolor='green'>Good Condition</td>";
                        _texto += "                                                                                              <td bgcolor='red'>Bad Condition</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Why is it a problem?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_2 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td rowspan='6'><img src ='"+_rutaHttpcondicion_buena + "' alt='Imagen Buena' height='300' width='300'/> </td>";
                        _texto += "                                                                                              <td rowspan='6'><img src ='"+_rutaHttpcondicion_mala+ "' alt='Imagen Buena' height='300' width='300'/></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>When did it happen?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_3 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Who has detected it?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_4 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Where has it been detected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_5 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>How has it been detected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_6 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>How many?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_7 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4' bgcolor='silver'><strong>STATUS of the Red Alert(Quality)</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'>Describe containment inspection method in house/customer:</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'>" + Alerta.Estatus_Descripcion + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of rejected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_1 + "</td>";
                        _texto += "                                                                                              <td>Corrective Action Initiated:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_1 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of inspected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_2 + "</td>";
                        _texto += "                                                                                              <td>Customer afectation?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_2 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of produced parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_3 + "</td>";
                        _texto += "                                                                                              <td>Other AAM plant afected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_3 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of suspected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_4 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                               </table>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>Please Approve the Alert in a timely manner. You can see it on the Home Page of Mexico: <a href='"+strDominio_Login+"'>RED ALERT</a></td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "</table>";

                        String Email = Correo.Email;
                        if (Correo.Nombre.Contains("-"))
                        {
                            Cls_Active_Directory AD = new Cls_Active_Directory();
                            String Apellidos = Correo.Nombre.Split('-').GetValue(1).ToString().Replace(" ", "");
                            String Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Trim();
                            var Lst_Usuarios = AD.SearchActiveDirectory(Apellidos);
                            if (Lst_Usuarios.Count > 0)
                            {
                                var Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                if (Lst_Usuario != null)
                                {
                                    Email = Lst_Usuario.EmailAddress;
                                }
                                else
                                {
                                    Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Split(' ').GetValue(0).ToString().Trim();
                                    Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                    if (Lst_Usuario != null)
                                    {
                                        Email = Lst_Usuario.EmailAddress;
                                    }
                                    else
                                    {
                                        Email = Correo.Email;
                                    }
                                }
                            }
                            else
                            {
                                Apellidos = Correo.Nombre.Split('-').GetValue(1).ToString().Split(' ').GetValue(0).ToString().Trim();
                                Lst_Usuarios = AD.SearchActiveDirectory(Apellidos);
                                if (Lst_Usuarios.Count > 0)
                                {
                                    var Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                    if (Lst_Usuario != null)
                                    {
                                        Email = Lst_Usuario.EmailAddress;
                                    }
                                    else
                                    {
                                        Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Split(' ').GetValue(0).ToString().Trim();
                                        Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                        if (Lst_Usuario != null)
                                        {
                                            Email = Lst_Usuario.EmailAddress;
                                        }
                                        else
                                        {
                                            Email = Correo.Email;
                                        }
                                    }
                                }
                                else
                                {
                                    Email = Correo.Email;
                                }
                            }
                        }

                        Enviado = Enviar_Mail(Correo.Email, null, _asunto, _texto);

                        if (Email.Trim().ToLower() != Correo.Email.Trim().ToLower())
                        {
                            using (var dbContext = new AAM_Red_AlertEntities1())
                            {
                                var Responsable = dbContext.Cat_Responsables_Autorizacion.Where(e => e.Responsable_ID == Correo.Responsable_ID).First();
                                Responsable.Email = Email;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        public string Enviar_Correo_Rechazado(List<Cls_Cat_Responsables_Autorizacion_Negocio> Lst_Responsables, Ope_Alertas_Rojas Alerta)
        {
            string Json_Resultado = String.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            //String[] archivos = new String[lst_Archivos.Count()];
            string ruta_archivo = string.Empty;
            //string ruta_logo = string.Empty;
            bool Enviado = false;
            string _asunto = "";
            string _texto = "";
            string Nombre_Planta = "";
            string Unidad_Negocios = "";
            string Nombre_Producto = "";
            string Nombre_Cliente = "";
            string Nombre_Area = "";
            string Nombre_Turno = "";
            string Nombre_Estatus = "";
            string strDominio_logo = System.Configuration.ConfigurationManager.AppSettings["http_ruta_logo"];
            string strDominio_Imagenes = System.Configuration.ConfigurationManager.AppSettings["http_ruta_imagenes"];
            string strDominio_Imagenes_buenas = System.Configuration.ConfigurationManager.AppSettings["http_ruta_imagen_buena"];
            string strDominio_Login = System.Configuration.ConfigurationManager.AppSettings["http_ruta_login"];
            string _rutaHtpplogo = "";
            _rutaHtpplogo = strDominio_logo + "logo_AAM.png";



            string _rutaHttpcondicion_buena = "";
            string nombre_condicion_buena = Alerta.Condicion_Buena;
            string[] nombre = nombre_condicion_buena.Split('/');
            _rutaHttpcondicion_buena = strDominio_Imagenes_buenas + nombre.GetValue(nombre.Length - 1);

            string _rutaHttpcondicion_mala = "";
            string nombre_condicion_mala = Alerta.Condicion_Mala;
            string[] nombres = nombre_condicion_mala.Split('/');
            _rutaHttpcondicion_mala = strDominio_Imagenes + nombres.GetValue(nombres.Length - 1);


            try
            {
                Cls_Base64_Image Base64_Image = new Cls_Base64_Image();

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var Plantas = dbContext.Apl_Plantas.Where(e => e.Planta_ID == Alerta.Planta_ID).First();
                    Nombre_Planta = Plantas.Nombre;
                    var Unidades_Negocios = dbContext.Cat_Unidades_Negocio.Where(e => e.Unidad_Negocio_ID == Alerta.Unidad_Negocio_ID).First();
                    Unidad_Negocios = Unidades_Negocios.Nombre;
                    var Productos = dbContext.Cat_Productos.Where(e => e.Producto_ID == Alerta.Producto_ID).First();
                    Nombre_Producto = Productos.Nombre;
                    var Clientes = dbContext.Cat_Clientes.Where(e => e.Cliente_ID == Alerta.Cliente_ID).First();
                    Nombre_Cliente = Clientes.Nombre;
                    var Areas = dbContext.Cat_Areas.Where(e => e.Area_ID == Alerta.Area_ID).First();
                    Nombre_Area = Areas.Nombre;
                    var Turnos = dbContext.Cat_Turnos.Where(e => e.Turno_ID == Alerta.Turno_ID).First();
                    Nombre_Turno = Turnos.Nombre;
                    var Estatus = dbContext.Apl_Estatus.Where(e => e.Estatus_ID == Alerta.Estatus_ID).First();
                    Nombre_Estatus = Estatus.Estatus;
                }
                //foreach (var Alerta in Lst_Alertas)
                {
                    Mensaje.Titulo = "Send Red Alert " + Nombre_Estatus;
                    foreach (var Correo in Lst_Responsables)
                    {
                        //for (int _archivos = 0; _archivos < lst_Archivos.Count(); _archivos++)
                        //{
                        //    ruta_archivo = Server.MapPath("../" + lst_Archivos[_archivos].URL_Archivo);

                        //    if (File.Exists(ruta_archivo))
                        //    {
                        //        archivos[_archivos] = ruta_archivo;
                        //    }
                        //    else
                        //    {
                        //        Mensaje.Estatus = "error";
                        //        Mensaje.Mensaje = "<i class='fa fa-check' style='color: #00A41E;'></i>&nbsp;La orden cambio de proceso no se pudo enviar. No se encontró el archivo";
                        //    }
                        //}

                        _asunto = "Red Alert #" + Alerta.No_Alerta_Roja + " " + Nombre_Estatus;

                        //Estructura correo
                        //ruta_logo = Server.MapPath("../../../Recursos/img/logo_AAM.png");

                        _texto = "";
                        _texto += "<table>";
                        _texto += "   <tr>";
                        _texto += "         <td>";
                        _texto += "                   <img src ='"+_rutaHtpplogo+ "' alt='Imagen Buena' />";
                        _texto += "         </td>";
                        _texto += "         <td>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td>";
                        _texto += "         </td>";
                        _texto += "         <td align='right'>";
                        _texto += "                   <h3>Red Alert</h3>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <table>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>Please be informed about the following Quality / Launching concern:</td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>";
                        _texto += "                                </td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td>You need to log in to the <a href='"+strDominio_Login+"'>Red Alert System</a> to release the communication to the following levels by approving it.</td>";
                        _texto += "                      </tr>";
                        _texto += "                      <tr>";
                        _texto += "                                <td><p style='color:red;font-weight:bold'>Consider Red Alert " + Nombre_Estatus + " will not be released unless you approve it.</p></td>";
                        _texto += "                      </tr>";
                        _texto += "                 </table>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                                                               <table>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'  bgcolor='silver'><strong>GENERAL DATA</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>AAM Plant:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Planta + "</td>";
                        _texto += "                                                                                              <td>Date of information:</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Bussines Unit:</td>";
                        _texto += "                                                                                              <td>" + Unidad_Negocios + "</td>";
                        _texto += "                                                                                              <td>Date of update:</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Product Name:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Producto + "</td>";
                        _texto += "                                                                                              <td>1. Safety major issue</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>AAM part number:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Numero_Parte + "</td>";
                        _texto += "                                                                                              <td>2. Major project risk (Project issue that affect the SORP)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Cliente + "</td>";
                        _texto += "                                                                                              <td>3. Major risks/incidents (Recall, Soft recall, 0km, Rework)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer product reference:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Referencia_Producto + "</td>";
                        _texto += "                                                                                              <td>4. Major issue found at Critical Safety Operations (17 Processes)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Customer site(s):</td>";
                        _texto += "                                                                                              <td>" + Alerta.Sitio_Cliente + "</td>";
                        _texto += "                                                                                              <td>5. Metallic/metalurgycal failure</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Product Model:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Vehiculo + "</td>";
                        _texto += "                                                                                              <td>6. Customer/IATF certification lost</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Afected Process/Area:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Area + "</td>";
                        _texto += "                                                                                              <td>7. Major warranty concerns (generate recall or soft recall)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Related shift:</td>";
                        _texto += "                                                                                              <td>" + Nombre_Turno + "</td>";
                        _texto += "                                                                                              <td>8. Major internal concerns (scrap, machine breakdown, ..)</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>CAR#:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Numero_CAR + "</td>";
                        _texto += "                                                                                              <td>9. Supplier quality/delivery issue</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'  bgcolor='silver'><strong>PROBLEM DESCRIPTION (5W+2H)</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Refuse Description</td>";
                        _texto += "                                                                                              <td colspan='3' style='color:red'>" + Alerta.Observaciones + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>What happened?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_1 + "</td>";
                        _texto += "                                                                                              <td bgcolor='green'>Good Condition</td>";
                        _texto += "                                                                                              <td bgcolor='red'>Bad Condition</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Why is it a problem?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_2 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td rowspan='6'><img src ='"+_rutaHttpcondicion_buena+ "'  alt='Imagen Buena' height='300' width='300'/> </td>";
                        _texto += "                                                                                              <td rowspan='6'><img src ='"+_rutaHttpcondicion_mala+ "' alt='Imagen Buena' height='300' width='300'/></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>When did it happen?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_3 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Who has detected it?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_4 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Where has it been detected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_5 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>How has it been detected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_6 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>How many?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Descripcion_7 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4' bgcolor='silver'><strong>STATUS of the Red Alert(Quality)</strong></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'>Describe containment inspection method in house/customer:</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td colspan='4'>" + Alerta.Estatus_Descripcion + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of rejected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_1 + "</td>";
                        _texto += "                                                                                              <td>Corrective Action Initiated:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_1 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of inspected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_2 + "</td>";
                        _texto += "                                                                                              <td>Customer afectation?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_2 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of produced parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_3 + "</td>";
                        _texto += "                                                                                              <td>Other AAM plant afected?</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Condicion_3 + "</td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                                              <tr>";
                        _texto += "                                                                                              <td>Number of suspected parts:</td>";
                        _texto += "                                                                                              <td>" + Alerta.Estatus_Partes_4 + "</td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                                              <td></td>";
                        _texto += "                                                                              </tr>";
                        _texto += "                                                               </table>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>Please Approve the Alert in a timely manner. You can see it on the Home Page of Mexico: <a href='"+strDominio_Login+"'>RED ALERT</a></td>";
                        _texto += "   </tr>";
                        _texto += "   <tr>";
                        _texto += "         <td colspan='2'>";
                        _texto += "                   <hr>";
                        _texto += "         </td>";
                        _texto += "   </tr>";
                        _texto += "</table>";

                        String Email = Correo.Email;
                        if (Correo.Nombre.Contains("-"))
                        {
                            Cls_Active_Directory AD = new Cls_Active_Directory();
                            String Apellidos = Correo.Nombre.Split('-').GetValue(1).ToString().Replace(" ", "");
                            String Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Trim();
                            var Lst_Usuarios = AD.SearchActiveDirectory(Apellidos);
                            if (Lst_Usuarios.Count > 0)
                            {
                                var Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                if (Lst_Usuario != null)
                                {
                                    Email = Lst_Usuario.EmailAddress;
                                }
                                else
                                {
                                    Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Split(' ').GetValue(0).ToString().Trim();
                                    Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                    if (Lst_Usuario != null)
                                    {
                                        Email = Lst_Usuario.EmailAddress;
                                    }
                                    else
                                    {
                                        Email = Correo.Email;
                                    }
                                }
                            }
                            else
                            {
                                Apellidos = Correo.Nombre.Split('-').GetValue(1).ToString().Split(' ').GetValue(0).ToString().Trim();
                                Lst_Usuarios = AD.SearchActiveDirectory(Apellidos);
                                if (Lst_Usuarios.Count > 0)
                                {
                                    var Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                    if (Lst_Usuario != null)
                                    {
                                        Email = Lst_Usuario.EmailAddress;
                                    }
                                    else
                                    {
                                        Nombre = Correo.Nombre.Split('-').GetValue(0).ToString().Split(' ').GetValue(0).ToString().Trim();
                                        Lst_Usuario = Lst_Usuarios.FirstOrDefault(s => s.FirstName.Equals(Nombre));
                                        if (Lst_Usuario != null)
                                        {
                                            Email = Lst_Usuario.EmailAddress;
                                        }
                                        else
                                        {
                                            Email = Correo.Email;
                                        }
                                    }
                                }
                                else
                                {
                                    Email = Correo.Email;
                                }
                            }
                        }

                        Enviado = Enviar_Mail(Correo.Email, null, _asunto, _texto);

                        if (Email.Trim().ToLower() != Correo.Email.Trim().ToLower())
                        {
                            using (var dbContext = new AAM_Red_AlertEntities1())
                            {
                                var Responsable = dbContext.Cat_Responsables_Autorizacion.Where(e => e.Responsable_ID == Correo.Responsable_ID).First();
                                Responsable.Email = Email;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }


        public Boolean Enviar_Mail(string Para, string[] archivos, string asunto, string texto)
        {
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            string Json_Resultado = string.Empty;
            string Adjuntos = "";

            try
            {
                //adjunta los archivos
                if (archivos != null)
                {
                    foreach (string Adjunto in archivos)
                    {
                        Adjuntos += Adjunto + ";";
                    }
                    if (Adjuntos.EndsWith(";"))
                    {
                        Adjuntos = Adjuntos.Substring(0, Adjuntos.Length - 1);
                    }
                }
                
                using (AAM_Red_AlertEntities1 context = new AAM_Red_AlertEntities1())

                {
                    SqlParameter sqlprofile_name = new SqlParameter("@profile_name", "eAlerter");
                    SqlParameter sqlrecipients = new SqlParameter("@recipients", Para);
                    SqlParameter sqlbody = new SqlParameter("@body", texto);
                    SqlParameter sqlsubject = new SqlParameter("@subject ", asunto);
                    SqlParameter sqlfile_attachments = new SqlParameter("@file_attachments ", Adjuntos);

                    var envio_mail = context.Database
                            .SqlQuery<Cls_Envio_Correo_Negocio>("Enivar_Correo @profile_name, @recipients, @body, @subject, @file_attachments",
                                    sqlprofile_name,
                                    sqlrecipients,
                                    sqlbody,
                                    sqlsubject,
                                    sqlfile_attachments)
                           .ToList();

                    Json_Resultado = JsonMapper.ToJson(envio_mail);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }

            return true;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Reporte_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alertas = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Report";
                Obj_Alertas = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {

                    //Reporte
                    String reporte = "Report_Alertas_Rojas";
                    String strParametros = "&rc:Parameters=false&rc:DocMap=false";
                    strParametros += "&No_Alerta_Roja=" + (Obj_Alertas.No_Alerta_Roja == 0 ? null : Obj_Alertas.No_Alerta_Roja.ToString());

                    String strReporte = ConfigurationManager.AppSettings["Report_Server"] + ConfigurationManager.AppSettings["Report_Folder"] + reporte + strParametros;

                    String popupScript = "<script language='javascript'> var oWin = window.open('" + strReporte + "','','menubar=no,toolbar=no,resizable=yes') ; if (oWin==null) alert('Disable Popup Blocker to Allow Reports to Run');</script>";

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "Report successfully generated.";
                    Mensaje.Mensaje += popupScript;


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Nivel(string jsonObject)
        {
            Cls_Cat_Responsables_Autorizacion_Negocio Obj_Responsable = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Responsables_Autorizacion_Negocio> Lista_Responsables = new List<Cls_Cat_Responsables_Autorizacion_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Responsable = JsonMapper.ToObject<Cls_Cat_Responsables_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Responsable = (from _responsable in dbContext.Cat_Responsables_Autorizacion
                                       join _criterios in dbContext.Cat_Criterios_Autorizacion on _responsable.Criterio_Autorizacion_ID equals _criterios.Criterio_Autorizacion_ID
                                       where (_criterios.Tipo_Criterio == "Quality" &&
                                       _criterios.Nombre == "Nivel 4" &&
                                       _responsable.Nombre == Cls_Sesiones.Empleado)
                                       //(_criterios.Tipo_Criterio == "Launcher" &&
                                       //_responsable.Nombre == Cls_Sesiones.Empleado)
                                       select new Cls_Cat_Responsables_Autorizacion_Negocio
                                       {
                                           No_Empleado = _responsable.No_Empleado,
                                           Nombre = _responsable.Nombre
                                       }).OrderByDescending(u => u.Nombre);

                    if (Responsable.Any())
                    {
                        Mensaje.Mensaje = (true).ToString();
                    }
                    else
                    {
                        Mensaje.Mensaje = (false).ToString();
                    }
                    // Json_Resultado = JsonMapper.ToJson(Lista_Responsables);

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Comentarios(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Comentarios_Detalles obj_comentarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles> Lista_Detalles = new List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_comentarios = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Alertas_Rojas_Comentarios_Detalles
                                       where _detalle.No_Alerta_Roja.Equals(obj_comentarios.No_Alerta_Roja)

                                       select new Cls_Ope_Alertas_Rojas_Comentarios_Detalles
                                       {
                                           No_Alerta_Roja = _detalle.No_Alerta_Roja,
                                           No_Alerta_Roja_Comentario_Detalles = _detalle.No_Alerta_Roja_Comentario_Detalles,
                                           Usuario_Creo = _detalle.Usuario_Creo,
                                           Comentario = _detalle.Comentario,
                                           Fecha_Creo = _detalle.Fecha_Creo.ToString()

                                       }).OrderByDescending(u => u.Fecha_Creo);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// Método para comentar una alerta
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Comentar_Alerta(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Comentarios_Detalles Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles> lst_Comentarios = new List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>();

            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>(jsonObject);
                lst_Comentarios = JsonConvert.DeserializeObject<List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>>(Obj_Alerta_Roja.Datos_Detalles_Comentarios);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var total_comentarios = (from _comentarios in dbContext.Ope_Alertas_Rojas_Comentarios_Detalles
                                             where _comentarios.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)
                                             select _comentarios).ToList();

                    int _total = total_comentarios.Count();

                    if (_total < 60)
                    {
                        foreach (var Detalles in lst_Comentarios)
                        {
                            Ope_Alertas_Rojas_Comentarios_Detalles det_comentarios = new Ope_Alertas_Rojas_Comentarios_Detalles();
                            det_comentarios.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                            det_comentarios.No_Alerta_Roja = Obj_Alerta_Roja.No_Alerta_Roja;
                            det_comentarios.No_Empleado = Cls_Sesiones.Datos_Empleados.No_Empleado;
                            det_comentarios.Comentario = Detalles.Comentario;
                            det_comentarios.Usuario_Creo = Cls_Sesiones.Usuario != "" ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                            det_comentarios.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                            if (det_comentarios.Comentario != "")
                                dbContext.Ope_Alertas_Rojas_Comentarios_Detalles.Add(det_comentarios);
                        }
                        dbContext.SaveChanges();
                        Mensaje.Estatus = "success";
                        Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Daily status up date." + " <br />";
                    }                  
                    else {

                        Mensaje.Estatus = "error";
                        Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp;Can not add this comment. Exceeds the maximum of comments." + " <br />";
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Comentario_Por_Empleado(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Comentarios_Detalles obj_comentarios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles> Lista_Detalles = new List<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                obj_comentarios = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var lst_detalle = (from _detalle in dbContext.Ope_Alertas_Rojas_Comentarios_Detalles

                                       where _detalle.No_Alerta_Roja.Equals(obj_comentarios.No_Alerta_Roja)
                                       && _detalle.Usuario_Creo.Equals(Cls_Sesiones.Empleado)

                                       select new Cls_Ope_Alertas_Rojas_Comentarios_Detalles
                                       {
                                           No_Alerta_Roja = _detalle.No_Alerta_Roja,
                                           No_Alerta_Roja_Comentario_Detalles = _detalle.No_Alerta_Roja_Comentario_Detalles,
                                           Usuario_Creo = _detalle.Usuario_Creo,
                                           Comentario = _detalle.Comentario

                                       }).OrderByDescending(u => u.No_Alerta_Roja);

                    foreach (var p in lst_detalle)
                        Lista_Detalles.Add(p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Detalles);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Validar_Total_Comentarios(string jsonObject)
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            Cls_Ope_Alertas_Rojas_Comentarios_Detalles Obj_Alerta_Roja = null;

            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Comentarios_Detalles>(jsonObject);
                using (var dbContext = new AAM_Red_AlertEntities1())
                {
                    var total_comentarios = (from _comentarios in dbContext.Ope_Alertas_Rojas_Comentarios_Detalles
                                             where _comentarios.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)
                                             select _comentarios).ToList();

                    int _total = total_comentarios.Count();

                    if (_total < 60)
                    {
                        Mensaje.Mensaje = (true).ToString();
                    }
                    else
                    {
                        Mensaje.Mensaje = (false).ToString();
                    }

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }


        #endregion
    }

}

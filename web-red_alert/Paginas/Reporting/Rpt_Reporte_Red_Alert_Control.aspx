﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.Master" AutoEventWireup="true" CodeBehind="Rpt_Reporte_Red_Alert_Control.aspx.cs" Inherits="web_red_alert.Paginas.Reporting.Rpt_Reporte_Red_Alert_Control" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Recursos/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <link href="../../Recursos/estilos/demo_form.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-combo/select2.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table-current/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../Recursos/icon-picker/css/icon-picker.css" rel="stylesheet" />
    <link href="../../Recursos/estilos/css_producto.css" rel="stylesheet" />
    <script src="../../Recursos/plugins/parsley.js"></script>
    <script src="../../Recursos/bootstrap-table-current/bootstrap-table.js"></script>

    <script src="../../Recursos/plugins/jquery.formatCurrency-1.4.0.min.js"></script>
    <script src="../../Recursos/plugins/jquery.formatCurrency.all.js"></script>
    <script src="../../Recursos/plugins/accounting.min.js"></script>
    <script src="../../Recursos/plugins/pinch.js"></script>

    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <script src="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
    <script src="../../Recursos/bootstrap-table-current/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../Recursos/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-editable.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>
    <script src="../../Recursos/icon-picker/js/iconPicker.js"></script>
    <script src="../../Recursos/javascript/seguridad/Js_Controlador_Sesion.js"></script>
    <script src="../../Recursos/plugins/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script src="../../Recursos/bootstrap-combo/select2.js"></script>
    <script src="../../Recursos/plugins/URI.min.js"></script>
    <script src="../../Recursos/javascript/reporting/Js_Rpt_Reporte_Red_Alert_Control.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid" style="height: 100vh;">

        <div class="page-header" align="left">
            <h3 style="font-family: 'Roboto Light', cursive !important; font-size: 24px; font-weight: bold; color: #808080;">Red Alert Control Report </h3>
        </div>

        <div class="panel panel-color panel-info collapsed" id="panel1">
            <div class="panel-heading filter">
                <h3 class="panel-title">
                    <i style="color: white;" class="glyphicon glyphicon-filter"></i>&nbsp;Search Filters
                </h3>
                <div class="panel-options">
                    <a id="ctrl_panel" href="#" data-toggle="panel">
                        <span class="collapse-icon">–</span>
                        <span class="expand-icon">+</span>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="cmb_busqueda_por_cliente" class="fuente_lbl_controles">Customer:</label>
                            <select id="cmb_busqueda_por_cliente" class="form-control" ></select>
                        </div>
                        <div class="col-md-4">
                            <label for="cmb_busqueda_por_numero_parte" class="fuente_lbl_controles">Part number:</label>
                            <select id="cmb_busqueda_por_numero_parte" class="form-control"></select>
                        </div>
                        <div class="col-md-4">
                            <label for="txt_busqueda_por_descripcion_1" class="fuente_lbl_controles">What happened?</label>
                            <input type="text" id="txt_busqueda_por_descripcion_1" class="form-control" placeholder="Search by what happened" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label  class="fuente_lbl_controles">Affected Area:</label>
                            <select id="cmb_busqueda_por_area" class="form-control"></select>
                        </div>
                        <div class="col-md-4">
                            <label class="fuente_lbl_controles">Status:</label>
                            <select id="cmb_busqueda_por_estatus" class="form-control"></select>
                        </div>
                        <div class="col-md-4" align="right" style="margin-top:7px">
                            <button type="button" id="btn_busqueda" class="btn btn-secondary btn-icon btn-icon-standalone btn-lg">
                                <i class="fa fa-search"></i>
                                <span>Search</span>
                            </button>
                        </div>   
                    </div>
                  <div class="row"></br></div>
                    <div class="row">
                        <table id="tbl_alertas_rojas" class="table table-responsive" style="display:none"></table>
                    </div>
                </div>
        </div>
        <div class="panel-body">
            <div class="col-md-3 col-xs-3">
                <label class="text-bold text-left text-medium fuente_lbl_controles" title="Start date" style="margin-bottom: 5px !important;">(*) Start date</label>
                <div class="input-group date" id="dtp_fecha_inicio">
                    <input type="text" id="f_inicio_1" class="form-control" style="margin: 0px;" placeholder="dd/mm/aaaa" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-3 col-xs-3">
                <label class="text-bold text-left text-medium fuente_lbl_controles" title="End date" style="margin-bottom: 5px !important;">(*) End date</label>
                <div class="input-group date" id="dtp_fecha_termino">
                    <input type="text" id="f_termino_1" class="form-control" style="margin: 0px;" placeholder="dd/mm/aaaa" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div style="margin-top: 18px !important; text-align: left !important;">
                    <button type="button" id="btn_imprimir_control_alertas" class="btn btn-secondary btn-icon btn-icon-standalone btn-lg" style="width: 90% !important;">
                        <i class="fa fa-print"></i>
                        <span>Print report</span>
                    </button>
                </div>
            </div>
        </div>
        
    </div>
</asp:Content>


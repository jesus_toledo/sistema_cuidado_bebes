﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Negocio;
using datos_red_alert;
using web_red_alert.Models.Ayudante;
using System.Collections.Specialized;

namespace web_red_alert.Paginas.Reporting.controllers
{
    /// <summary>
    /// Descripción breve de Rpt_Reporte_Red_Alert_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Rpt_Reporte_Red_Alert_Controller : System.Web.Services.WebService
    {
        #region Métodos
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Reporte_Control_Alertas(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta = null;
            //  Cls_Red_Alert_Negocio Obj_Cambio = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {

                Mensaje.Titulo = "Report";
                Obj_Alerta = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);
                using (var dbContext = new AAM_Red_AlertEntities())
                {

                    //Reporte
                    String reporte = "Red_Alert_Control";
                    String strParametros = "&rc:Parameters=false&rc:DocMap=false";
                    strParametros += "&Inicio=" + Obj_Alerta.Fecha_Creo.ToString("yyyyMMdd");
                    strParametros += "&Fin=" + Obj_Alerta.Fecha_Creo1.AddDays(1).ToString("yyyyMMdd");

                    String strReporte = ConfigurationManager.AppSettings["Report_Server"] + ConfigurationManager.AppSettings["Report_Folder"] + reporte + strParametros;

                    String popupScript = "<script language='javascript'> var oWin = window.open('" + strReporte + "','','menubar=no,toolbar=no,resizable=yes') ; if (oWin==null) alert('Disable Popup Blocker to Allow Reports to Run');</script>";

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The report was generated successfully.";
                    Mensaje.Mensaje += popupScript;
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;

        }
        /// <summary>
        /// Método para consultar las areas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Areas()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Areas = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_areas = (from _areas in dbContext.Cat_Areas
                                      where _areas.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                      && _areas.Nombre.Contains(q)
                                      select new Cls_Select2
                                      {
                                          id = _areas.Area_ID.ToString(),
                                          text = _areas.Nombre,
                                          tag = String.Empty
                                      }).OrderBy(u => u.text);

                    if (_lst_areas.Any())
                        foreach (var p in _lst_areas)
                            Lista_Areas.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Areas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// Método para consultar los numeros de partes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Numero_Partes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Numero_Partes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_partes = (from _partes in dbContext.Cat_Numero_Partes
                                       where _partes.Nombre.Contains(q)
                                       select new Cls_Select2
                                       {
                                           id = _partes.Numero_Parte_ID.ToString(),
                                           text = _partes.Nombre,
                                           tag = String.Empty
                                       }).OrderBy(u => u.text);

                    if (_lst_partes.Any())
                        foreach (var p in _lst_partes)
                            Lista_Numero_Partes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Numero_Partes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los clientes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Clientes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Clientes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_clientes = (from _clientes in dbContext.Cat_Clientes
                                         where _clientes.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _clientes.Nombre.Contains(q)
                                         select new Cls_Select2
                                         {
                                             id = _clientes.Cliente_ID.ToString(),
                                             text = _clientes.Nombre,
                                             tag = String.Empty
                                         }).OrderBy(u => u.text);

                    if (_lst_clientes.Any())
                        foreach (var p in _lst_clientes)
                            Lista_Clientes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los estatus
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Estatus = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_estatus = (from _estatus in dbContext.Apl_Estatus
                                        where _estatus.Estatus.Contains(q)
                                        select new Cls_Select2
                                        {
                                            id = _estatus.Estatus_ID.ToString(),
                                            text = _estatus.Estatus,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);
                    if (_lst_estatus.Any())
                        foreach (var p in _lst_estatus)
                            Lista_Estatus.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Estatus);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para generar el reporte de la alerta
        /// </summary>
        /// <returns>Reporte</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Reporte_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alertas = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Report";
                Obj_Alertas = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {

                    //Reporte
                    String reporte = "Report_Alertas_Rojas";
                    String strParametros = "&rc:Parameters=false&rc:DocMap=false";
                    strParametros += "&No_Alerta_Roja=" + (Obj_Alertas.No_Alerta_Roja == 0 ? null : Obj_Alertas.No_Alerta_Roja.ToString());

                    String strReporte = ConfigurationManager.AppSettings["Report_Server"] + ConfigurationManager.AppSettings["Report_Folder"] + reporte + strParametros;

                    String popupScript = "<script language='javascript'> var oWin = window.open('" + strReporte + "','','menubar=no,toolbar=no,resizable=yes') ; if (oWin==null) alert('Disable Popup Blocker to Allow Reports to Run');</script>";

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "Report successfully generated.";
                    Mensaje.Mensaje += popupScript;
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Alertas_Rojas(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_alertas = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try

            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _alertas in dbContext.Ope_Alertas_Rojas
                                   join _estatus in dbContext.Apl_Estatus on _alertas.Estatus_ID equals _estatus.Estatus_ID
                                   join _clientes in dbContext.Cat_Clientes on _alertas.Cliente_ID equals _clientes.Cliente_ID
                                   join _areas in dbContext.Cat_Areas on _alertas.Area_ID equals _areas.Area_ID
                                   from _numero_parte in dbContext.Cat_Numero_Partes.Where(x => x.Numero_Parte_ID == _alertas.Numero_Parte_ID).DefaultIfEmpty()

                                   where
                                    (Obj_Alerta_Roja.Estatus_ID != 0 ? _alertas.Estatus_ID == (Obj_Alerta_Roja.Estatus_ID) : true) &&
                                    (!string.IsNullOrEmpty(Obj_Alerta_Roja.Descripcion_1) ? _alertas.Descripcion_1.ToLower().Contains(Obj_Alerta_Roja.Descripcion_1.ToLower()) : true) &&
                                    (Obj_Alerta_Roja.Area_ID != 0 && Obj_Alerta_Roja.Area_ID != null ? _areas.Area_ID == (Obj_Alerta_Roja.Area_ID) : true) &&
                                    (Obj_Alerta_Roja.Cliente_ID != 0 && Obj_Alerta_Roja.Cliente_ID != null ? _clientes.Cliente_ID == Obj_Alerta_Roja.Cliente_ID : true) &&
                                    //(Obj_Alerta_Roja.Numero_Parte_ID != 0 && Obj_Alerta_Roja.Numero_Parte_ID!=null ? _numero_parte.Numero_Parte_ID == Obj_Alerta_Roja.Numero_Parte_ID : true)&&
                                    //(!string.IsNullOrEmpty(Obj_Alerta_Roja.Numero_Parte) && Obj_Alerta_Roja.Numero_Parte_ID !=null && Obj_Alerta_Roja.Numero_Parte_ID==0 ? _alertas.Numero_Parte.ToLower().Contains(Obj_Alerta_Roja.Numero_Parte.ToLower()) && _alertas.Numero_Parte_ID == (Obj_Alerta_Roja.Numero_Parte_ID) : true)
                                    (Obj_Alerta_Roja.Numero_Parte_ID != null && Obj_Alerta_Roja.Numero_Parte_ID != 0 ? _alertas.Numero_Parte_ID == Obj_Alerta_Roja.Numero_Parte_ID : true) &&
                                    (Obj_Alerta_Roja.Numero_Parte != null && Obj_Alerta_Roja.Numero_Parte != "" ? _alertas.Numero_Parte == Obj_Alerta_Roja.Numero_Parte : true)
                                   //(Obj_Alerta_Roja.Numero_Parte !=null && Obj_Alerta_Roja.Numero_Parte_ID == 0 ? _alertas.Numero_Parte==(Obj_Alerta_Roja.Numero_Parte) && _numero_parte.Numero_Parte_ID==(Obj_Alerta_Roja.Numero_Parte_ID) : true) 
                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alertas.No_Alerta_Roja,
                                       Descripcion_1 = _alertas.Descripcion_1,
                                       Estatus = _estatus.Estatus,
                                       Cliente = _clientes.Nombre,
                                       Area = _areas.Nombre,
                                       Numero_Parte = _alertas.Numero_Parte != null ? _alertas.Numero_Parte : _numero_parte.Nombre,


                                   }).OrderByDescending(u => u.No_Alerta_Roja);

                    foreach (var p in Alertas)
                        Lista_alertas.Add((Cls_Ope_Alertas_Rojas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        #endregion
    }
}

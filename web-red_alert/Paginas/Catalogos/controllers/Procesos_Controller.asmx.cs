﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Procesos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Procesos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta del proceso.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Procesos_Negocio Obj_Procesos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Procesos = JsonMapper.ToObject<Cls_Cat_Procesos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _procesos = new Cat_Procesos();
                    _procesos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _procesos.Estatus_ID = Obj_Procesos.Estatus_ID;
                    _procesos.Nombre = Obj_Procesos.Nombre;
                    _procesos.Observaciones = Obj_Procesos.Observaciones;
                    _procesos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _procesos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Procesos.Add(_procesos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Some of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos del proceso seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Procesos_Negocio Obj_Procesos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Procesos = JsonMapper.ToObject<Cls_Cat_Procesos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _procesos = dbContext.Cat_Procesos.Where(u => u.Proceso_ID == Obj_Procesos.Proceso_ID).First();

                    _procesos.Estatus_ID = Obj_Procesos.Estatus_ID;
                    _procesos.Nombre = Obj_Procesos.Nombre;
                    _procesos.Observaciones = Obj_Procesos.Observaciones;
                    _procesos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _procesos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Some of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Procesos_Negocio Obj_Procesos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Procesos = JsonMapper.ToObject<Cls_Cat_Procesos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _procesos = dbContext.Cat_Procesos.Where(u => u.Proceso_ID == Obj_Procesos.Proceso_ID).First();
                    dbContext.Cat_Procesos.Remove(_procesos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _proceso = dbContext.Cat_Procesos.Where(u => u.Proceso_ID == Obj_Procesos.Proceso_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _proceso.Estatus_ID = _estatus.Estatus_ID;
                            _proceso.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _proceso.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del proceso.
        /// <returns>Listado de los procesos filtrados por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Procesos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Procesos_Negocio Obj_Procesos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Procesos_Negocio> Lista_Empaques = new List<Cls_Cat_Procesos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Procesos = JsonMapper.ToObject<Cls_Cat_Procesos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _procesos = (from _proceso in dbContext.Cat_Procesos
                                  join _estatus in dbContext.Apl_Estatus on _proceso.Estatus_ID equals _estatus.Estatus_ID

                                  where _proceso.Proceso_ID.Equals(Obj_Procesos.Proceso_ID) ||
                                  _proceso.Nombre.Equals(Obj_Procesos.Nombre)

                                  select new Cls_Cat_Procesos_Negocio
                                  {
                                      Proceso_ID = _proceso.Proceso_ID,
                                      Nombre = _proceso.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_procesos.Any())
                    {
                        if (Obj_Procesos.Proceso_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Procesos.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _procesos.Where(u => u.Proceso_ID == Obj_Procesos.Proceso_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Procesos.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de los procesos.
        /// </summary>
        /// <returns>Listado serializado con los procesos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Procesos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Procesos_Negocio Obj_Procesos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Procesos_Negocio> Lista_procesos = new List<Cls_Cat_Procesos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Procesos = JsonMapper.ToObject<Cls_Cat_Procesos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Procesos = (from _procesos in dbContext.Cat_Procesos
                                 join _estatus in dbContext.Apl_Estatus on _procesos.Estatus_ID equals _estatus.Estatus_ID

                                 where _procesos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Procesos.Nombre) ? _procesos.Nombre.ToLower().Contains(Obj_Procesos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Procesos_Negocio
                                 {
                                     Proceso_ID = _procesos.Proceso_ID,
                                     Nombre = _procesos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _procesos.Estatus_ID,
                                     Empresa_ID = _procesos.Empresa_ID,
                                     Observaciones = _procesos.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Procesos)
                        Lista_procesos.Add((Cls_Cat_Procesos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_procesos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

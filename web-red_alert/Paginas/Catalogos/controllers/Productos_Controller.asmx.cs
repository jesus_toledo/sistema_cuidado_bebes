﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Productos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Productos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la producto.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Productos_Negocio Obj_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _productos = new Cat_Productos();
                    _productos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _productos.Estatus_ID = Obj_Productos.Estatus_ID;
                    _productos.Nombre = Obj_Productos.Nombre;
                    _productos.Observaciones = Obj_Productos.Observaciones;
                    _productos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _productos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Productos.Add(_productos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la producto seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Productos_Negocio Obj_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _productos = dbContext.Cat_Productos.Where(u => u.Producto_ID == Obj_Productos.Producto_ID).First();

                    _productos.Estatus_ID = Obj_Productos.Estatus_ID;
                    _productos.Nombre = Obj_Productos.Nombre;
                    _productos.Observaciones = Obj_Productos.Observaciones;
                    _productos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _productos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Productos_Negocio Obj_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _productos = dbContext.Cat_Productos.Where(u => u.Producto_ID == Obj_Productos.Producto_ID).First();
                    dbContext.Cat_Productos.Remove(_productos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _producto = dbContext.Cat_Productos.Where(u => u.Producto_ID == Obj_Productos.Producto_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _producto.Estatus_ID = _estatus.Estatus_ID;
                            _producto.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _producto.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la producto.
        /// <returns>Listado de las productos  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Productos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Productos_Negocio Obj_Productos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Productos_Negocio> Lista_Empaques = new List<Cls_Cat_Productos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _productos = (from _producto in dbContext.Cat_Productos
                                      join _estatus in dbContext.Apl_Estatus on _producto.Estatus_ID equals _estatus.Estatus_ID

                                      where _producto.Producto_ID.Equals(Obj_Productos.Producto_ID) ||
                                      _producto.Nombre.Equals(Obj_Productos.Nombre)

                                      select new Cls_Cat_Productos_Negocio
                                      {
                                          Producto_ID = _producto.Producto_ID,
                                          Nombre = _producto.Nombre,
                                          Estatus = _estatus.Estatus
                                      }).OrderByDescending(u => u.Nombre);

                    if (_productos.Any())
                    {
                        if (Obj_Productos.Producto_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Productos.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _productos.Where(u => u.Producto_ID == Obj_Productos.Producto_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Productos.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las productos.
        /// </summary>
        /// <returns>Listado serializado con las productos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Productos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Productos_Negocio Obj_Productos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Productos_Negocio> Lista_productos = new List<Cls_Cat_Productos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Productos = (from _productos in dbContext.Cat_Productos
                                     join _estatus in dbContext.Apl_Estatus on _productos.Estatus_ID equals _estatus.Estatus_ID

                                     where _productos.Empresa_ID.Equals(empresa_id) &&
                                     _estatus.Estatus != "DELETED" &&
                                     (!string.IsNullOrEmpty(Obj_Productos.Nombre) ? _productos.Nombre.ToLower().Contains(Obj_Productos.Nombre.ToLower()) : true)

                                     select new Cls_Cat_Productos_Negocio
                                     {
                                         Producto_ID = _productos.Producto_ID,
                                         Nombre = _productos.Nombre,
                                         Estatus = _estatus.Estatus,
                                         Estatus_ID = _productos.Estatus_ID,
                                         Empresa_ID = _productos.Empresa_ID,
                                         Observaciones = _productos.Observaciones,
                                     }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Productos)
                        Lista_productos.Add((Cls_Cat_Productos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_productos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

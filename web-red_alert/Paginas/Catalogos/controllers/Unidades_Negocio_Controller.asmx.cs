﻿using 
    datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Unidades_Negocio_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Unidades_Negocio_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la unidad de negocio.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _unidades = new Cat_Unidades_Negocio();
                    _unidades.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _unidades.Planta_ID = Obj_Unidades_Negocio.Planta_ID;
                    _unidades.Estatus_ID = Obj_Unidades_Negocio.Estatus_ID;
                    _unidades.Nombre = Obj_Unidades_Negocio.Nombre;
                    _unidades.Observaciones = Obj_Unidades_Negocio.Observaciones;
                    _unidades.Usuario_Creo = Cls_Sesiones.Usuario;
                    _unidades.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Unidades_Negocio.Add(_unidades);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la unidad de negocio seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update data";
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _unidades = dbContext.Cat_Unidades_Negocio.Where(u => u.Unidad_Negocio_ID == Obj_Unidades_Negocio.Unidad_Negocio_ID).First();

                    _unidades.Planta_ID = Obj_Unidades_Negocio.Planta_ID;
                    _unidades.Estatus_ID = Obj_Unidades_Negocio.Estatus_ID;
                    _unidades.Nombre = Obj_Unidades_Negocio.Nombre;
                    _unidades.Observaciones = Obj_Unidades_Negocio.Observaciones;
                    _unidades.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _unidades.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _unidades = dbContext.Cat_Unidades_Negocio.Where(u => u.Unidad_Negocio_ID == Obj_Unidades_Negocio.Unidad_Negocio_ID).First();
                    dbContext.Cat_Unidades_Negocio.Remove(_unidades);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _unidad = dbContext.Cat_Unidades_Negocio.Where(u => u.Unidad_Negocio_ID == Obj_Unidades_Negocio.Unidad_Negocio_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _unidad.Estatus_ID = _estatus.Estatus_ID;
                            _unidad.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _unidad.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la unidad de negocio.
        /// <returns>Listado de las unidades de negocio  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Unidades_Negocio_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Unidades_Negocio_Negocio> Lista_Empaques = new List<Cls_Cat_Unidades_Negocio_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _unidades = (from _unidad in dbContext.Cat_Unidades_Negocio
                                  join _estatus in dbContext.Apl_Estatus on _unidad.Estatus_ID equals _estatus.Estatus_ID

                                  where _unidad.Unidad_Negocio_ID.Equals(Obj_Unidades_Negocio.Unidad_Negocio_ID) ||
                                  _unidad.Nombre.Equals(Obj_Unidades_Negocio.Nombre)

                                  select new Cls_Cat_Unidades_Negocio_Negocio
                                  {
                                      Unidad_Negocio_ID = _unidad.Unidad_Negocio_ID,
                                      Nombre = _unidad.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_unidades.Any())
                    {
                        if (Obj_Unidades_Negocio.Unidad_Negocio_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Unidades_Negocio.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _unidades.Where(u => u.Unidad_Negocio_ID == Obj_Unidades_Negocio.Unidad_Negocio_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Unidades_Negocio.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las unidades de negocio.
        /// </summary>
        /// <returns>Listado serializado con las unidades de negocio según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Unidades_Negocio_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Unidades_Negocio_Negocio> Lista_unidades = new List<Cls_Cat_Unidades_Negocio_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Unidades = (from _unidades in dbContext.Cat_Unidades_Negocio
                                 join _estatus in dbContext.Apl_Estatus on _unidades.Estatus_ID equals _estatus.Estatus_ID
                                 //join _plantas in dbContext.Apl_Plantas on new { planta = _unidades.Planta_ID, empresa = _unidades.Empresa_ID }
                                 //equals new { planta = _plantas.Sucursal_ID, empresa = _plantas.Empresa_ID }

                                 where _unidades.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Unidades_Negocio.Nombre) ? _unidades.Nombre.ToLower().Contains(Obj_Unidades_Negocio.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Unidades_Negocio_Negocio
                                 {
                                     Unidad_Negocio_ID = _unidades.Unidad_Negocio_ID,
                                     Nombre = _unidades.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _unidades.Estatus_ID,
                                     Empresa_ID = _unidades.Empresa_ID,
                                     Planta_ID = _unidades.Planta_ID,
                                     Observaciones = _unidades.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);

                    foreach (var p in Unidades)
                        Lista_unidades.Add((Cls_Cat_Unidades_Negocio_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_unidades);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las plantas.
        /// </summary>
        /// <returns>Listado serializado de las plantas</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Plantas()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Plantas> Lista_plantas = new List<Apl_Plantas>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Plantas = from _plantas in dbContext.Apl_Plantas
                                  where _plantas.Empresa_ID.Equals(empresa_id)
                                  select new { _plantas.Nombre, _plantas.Planta_ID };

                    Json_Resultado = JsonMapper.ToJson(Plantas.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Parametros_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Parametros_Controller : System.Web.Services.WebService
    {

        #region Metodos
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _parametros= new Apl_Parametros();
                    _parametros.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _parametros.Prefijo = ObjParametro.Prefijo;
                    _parametros.No_Intentos_Acceso = "3";
                    _parametros.Tipo_Usuario = ObjParametro.Tipo_Usuario;
                    _parametros.Menu_ID = ObjParametro.Menu_ID == 0 ? null : ObjParametro.Menu_ID;

                    dbContext.Apl_Parametros.Add(_parametros);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la actualización de los datos de la unidad seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _parametros = dbContext.Apl_Parametros.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID).First();

                    _parametros.Prefijo = ObjParametro.Prefijo;
                    _parametros.Tipo_Usuario = ObjParametro.Tipo_Usuario;
                    _parametros.Menu_ID = ObjParametro.Menu_ID == 0 ? null : ObjParametro.Menu_ID;

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _parametro = dbContext.Apl_Parametros.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID).First();
                    dbContext.Apl_Parametros.Remove(_parametro);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                    Mensaje.Mensaje =
                        "The delete record operation was revoked. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; The record you are trying to delete is already in use.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de Procesos.
        /// </summary>
        /// <returns>Listado de procesos filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametro_Por_prefijo(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio ObjParametro = null;
            string Json_Resultado = string.Empty;
            List<Cls_Tra_Cat_Parametros_Negocio> Lista_parametros = new List<Cls_Tra_Cat_Parametros_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                ObjParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _parametro = (from _select in dbContext.Apl_Parametros
                                      where _select.Parametro_ID.Equals(ObjParametro.Parametro_ID) ||
                                     _select.Prefijo.Equals(ObjParametro.Prefijo)
                                     select new Cls_Tra_Cat_Parametros_Negocio
                                     {
                                         Parametro_ID = _select.Parametro_ID,
                                         Prefijo = _select.Prefijo
                                     }).OrderByDescending(u => u.Parametro_ID);

                    if (_parametro.Any())
                    {
                        if (ObjParametro.Parametro_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(ObjParametro.Prefijo))
                                Mensaje.Mensaje = "The entered prefix is ​​already registered.";
                          
                        }
                        else
                        {
                            var item_edit = _parametro.Where(u => u.Parametro_ID == ObjParametro.Parametro_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(ObjParametro.Prefijo))
                                    Mensaje.Mensaje = "The entered prefix is ​​already registered.";
                              
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método que realiza la búsqueda de procesos.
        /// </summary>
        /// <returns>Listado serializado con las fases según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Parametro_Por_Filtros(string jsonObject)
        {
            Cls_Tra_Cat_Parametros_Negocio objParametro = null;
            string Json_Resultado = string.Empty;
            List<Cls_Tra_Cat_Parametros_Negocio> Lista_parametros = new List<Cls_Tra_Cat_Parametros_Negocio>();
            int empresa = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                objParametro = JsonMapper.ToObject<Cls_Tra_Cat_Parametros_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var parametro = (from _select in dbContext.Apl_Parametros
                                     where _select.Empresa_ID.Equals(empresa)&&(
                                        ((_select.Prefijo.ToLower().Contains(objParametro.Prefijo.ToLower()) && !string.IsNullOrEmpty(objParametro.Prefijo))) ||
                                        (string.IsNullOrEmpty(objParametro.Prefijo)))
                                     
                                    select new
                                    {
                                        Parametro_ID = _select.Parametro_ID,
                                        Prefijo = _select.Prefijo,
                                        Tipo_Usuario = _select.Tipo_Usuario,
                                        Empresa_ID = _select.Empresa_ID,
                                        Menu_ID=_select.Menu_ID,
                                        Nombre_Mostrar = (_select.Menu_ID == null) ? null : (dbContext.Apl_Menus.Where(e => e.Menu_ID == _select.Menu_ID).Select(e => e.Nombre_Mostrar).FirstOrDefault())

                                    }).OrderByDescending(u => u.Parametro_ID);

                    Json_Resultado = JsonMapper.ToJson(parametro.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConsultarTipoUsuario()
        {
            string Json_Resultado = string.Empty;
            List<Cls_Apl_Tipos_Usuarios_Negocio> Lista_fase = new List<Cls_Apl_Tipos_Usuarios_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var tipos_usuarios = from _select in dbContext.Apl_Tipos_Usuarios
                             
                                select new { _select.Tipo_Usuario_ID, _select.Nombre };


                    Json_Resultado = JsonMapper.ToJson(tipos_usuarios.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Menus()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var menus = from _menu in dbContext.Apl_Menus
                                  select new { _menu.Menu_ID, _menu.Nombre_Mostrar };
                    Json_Resultado = JsonMapper.ToJson(menus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        #endregion
    }
}

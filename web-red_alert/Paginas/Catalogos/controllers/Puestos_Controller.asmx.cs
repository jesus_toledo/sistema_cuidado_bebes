﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Puestos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Puestos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _puestos = new Cat_Puestos();
                    _puestos.Estatus_ID = Obj_Puestos.Estatus_ID;
                    _puestos.Area_ID = Obj_Puestos.Area_ID;
                    _puestos.Nombre = Obj_Puestos.Nombre;
                    _puestos.Descripcion = Obj_Puestos.Descripcion;
                    _puestos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _puestos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Puestos.Add(_puestos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update date";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _puestos = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();

                    _puestos.Estatus_ID = Obj_Puestos.Estatus_ID;
                    _puestos.Area_ID = Obj_Puestos.Area_ID;
                    _puestos.Nombre = Obj_Puestos.Nombre;
                    _puestos.Descripcion = Obj_Puestos.Descripcion;
                    _puestos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _puestos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _puestos = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();
                    dbContext.Cat_Puestos.Remove(_puestos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _puesto = dbContext.Cat_Puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _puesto.Estatus_ID = _estatus.Estatus_ID;
                            _puesto.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _puesto.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Puestos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Puestos_Negocio> Lista_Empaques = new List<Cls_Cat_Puestos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _puestos = (from _puesto in dbContext.Cat_Puestos
                                    join _estatus in dbContext.Apl_Estatus on _puesto.Estatus_ID equals _estatus.Estatus_ID
                                    join _areas in dbContext.Cat_Areas on _puesto.Area_ID equals _areas.Area_ID

                                    where _puesto.Area_ID.Equals(Obj_Puestos.Area_ID) &&
                                    _puesto.Nombre.Equals(Obj_Puestos.Nombre)

                                    select new Cls_Cat_Puestos_Negocio
                                    {
                                        Puesto_ID = _puesto.Puesto_ID,
                                        Nombre = _puesto.Nombre,
                                        Estatus = _estatus.Estatus,
                                        Area = _areas.Nombre,
                                        Area_ID = _puesto.Area_ID
                                    }).OrderByDescending(u => u.Nombre);

                    //if (Obj_Puestos.Area_ID != 0)
                    //{
                    //    _puestos = (from _puesto in dbContext.Cat_Puestos
                    //                join _estatus in dbContext.Apl_Estatus on _puesto.Estatus_ID equals _estatus.Estatus_ID
                    //                join _areas in dbContext.Cat_Areas on _puesto.Area_ID equals _areas.Area_ID

                    //                where (_puesto.Puesto_ID.Equals(Obj_Puestos.Puesto_ID) &&
                    //                _puesto.Area_ID.Equals(Obj_Puestos.Area_ID)) ||
                    //                (_puesto.Nombre.Equals(Obj_Puestos.Nombre) &&
                    //                _puesto.Area_ID.Equals(Obj_Puestos.Area_ID))

                    //                select new Cls_Cat_Puestos_Negocio
                    //                {
                    //                    Puesto_ID = _puesto.Puesto_ID,
                    //                    Nombre = _puesto.Nombre,
                    //                    Estatus = _estatus.Estatus,
                    //                    Area = _areas.Nombre,
                    //                    Area_ID = _puesto.Area_ID
                    //                }).OrderByDescending(u => u.Nombre);
                    //}

                    if (_puestos.Any())
                    {
                        if (Obj_Puestos.Puesto_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Puestos.Nombre))
                                Mensaje.Mensaje = "The name and area entered are already registered.";
                        }
                        else
                        {
                            var item_edit = _puestos.Where(u => u.Puesto_ID == Obj_Puestos.Puesto_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Puestos.Nombre))
                                    Mensaje.Mensaje = "The name and area entered are already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Puestos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Puestos_Negocio Obj_Puestos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Puestos_Negocio> Lista_puestos = new List<Cls_Cat_Puestos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Puestos = JsonMapper.ToObject<Cls_Cat_Puestos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Puestos = (from _puestos in dbContext.Cat_Puestos
                                 join _estatus in dbContext.Apl_Estatus on _puestos.Estatus_ID equals _estatus.Estatus_ID
                                 join _areas in dbContext.Cat_Areas on _puestos.Area_ID equals _areas.Area_ID

                                 where //_puestos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Puestos.Nombre) ? _puestos.Nombre.ToLower().Contains(Obj_Puestos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Puestos_Negocio
                                 {
                                     Puesto_ID = _puestos.Puesto_ID,
                                     Nombre = _puestos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _puestos.Estatus_ID,
                                     Area = _areas.Nombre,
                                     Area_ID = _puestos.Area_ID,
                                     Descripcion = _puestos.Descripcion,
                                 }).OrderByDescending(u => u.Nombre);
                    
                    foreach (var p in Puestos)
                        Lista_puestos.Add((Cls_Cat_Puestos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_puestos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                    
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las areas.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Areas()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Areas> Lista_areas = new List<Cat_Areas>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Areas = from _areas in dbContext.Cat_Areas
                                  select new { _areas.Nombre, _areas.Area_ID };

                    Json_Resultado = JsonMapper.ToJson(Areas.ToList());

                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

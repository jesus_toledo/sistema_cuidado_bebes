﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Descripción breve de Numero_Partes_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Numero_Partes_Controller : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Numero_Partes_Negocio Obj_Numero_Partes = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Numero_Partes = JsonMapper.ToObject<Cls_Cat_Numero_Partes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _numero_parte = new Cat_Numero_Partes();
                   
                   _numero_parte.Estatus_ID = Obj_Numero_Partes.Estatus_ID;
                    _numero_parte.Nombre = Obj_Numero_Partes.Nombre;
                    _numero_parte.Observaciones = Obj_Numero_Partes.Observaciones;
                    _numero_parte.Usuario_Creo = Cls_Sesiones.Usuario;
                    _numero_parte.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Numero_Partes.Add(_numero_parte);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
   
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Numero_Partes_Negocio Obj_Numero_Parte = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Numero_Parte = JsonMapper.ToObject<Cls_Cat_Numero_Partes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _numero_parte = dbContext.Cat_Numero_Partes.Where(u => u.Numero_Parte_ID == Obj_Numero_Parte.Numero_Parte_ID).First();

                    _numero_parte.Estatus_ID = Obj_Numero_Parte.Estatus_ID;
                    _numero_parte.Nombre = Obj_Numero_Parte.Nombre;
                    _numero_parte.Observaciones = Obj_Numero_Parte.Observaciones;
                    _numero_parte.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _numero_parte.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
              Cls_Cat_Numero_Partes_Negocio Obj_Numero_Parte = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Numero_Parte = JsonMapper.ToObject<Cls_Cat_Numero_Partes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _numero_parte = dbContext.Cat_Numero_Partes.Where(u => u.Numero_Parte_ID == Obj_Numero_Parte.Numero_Parte_ID).First();
                    dbContext.Cat_Numero_Partes.Remove(_numero_parte);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _numero_parte = dbContext.Cat_Numero_Partes.Where(u => u.Numero_Parte_ID == Obj_Numero_Parte.Numero_Parte_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _numero_parte.Estatus_ID = _estatus.Estatus_ID;
                            _numero_parte.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _numero_parte.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Numero_Parte_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Numero_Partes_Negocio Obj_Numero_Parte = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Numero_Partes_Negocio> Lista_Numero_Partes = new List<Cls_Cat_Numero_Partes_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Numero_Parte = JsonMapper.ToObject<Cls_Cat_Numero_Partes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _numero = (from _numero_parte in dbContext.Cat_Numero_Partes
                                  join _estatus in dbContext.Apl_Estatus on _numero_parte.Estatus_ID equals _estatus.Estatus_ID

                                  where _numero_parte.Numero_Parte_ID.Equals(Obj_Numero_Parte.Numero_Parte_ID) ||
                                  _numero_parte.Nombre.Equals(Obj_Numero_Parte.Nombre)

                                  select new Cls_Cat_Numero_Partes_Negocio
                                  {
                                      Numero_Parte_ID = _numero_parte.Numero_Parte_ID,
                                      Nombre = _numero_parte.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_numero.Any())
                    {
                        if (Obj_Numero_Parte.Numero_Parte_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Numero_Parte.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _numero.Where(u => u.Numero_Parte_ID == Obj_Numero_Parte.Numero_Parte_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Numero_Parte.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Numero_Parte_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Numero_Partes_Negocio Obj_Numero_Parte = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Numero_Partes_Negocio> Lista_areas = new List<Cls_Cat_Numero_Partes_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Numero_Parte = JsonMapper.ToObject<Cls_Cat_Numero_Partes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    //int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var numero_parte = (from _numero_parte in dbContext.Cat_Numero_Partes
                                 join _estatus in dbContext.Apl_Estatus on _numero_parte.Estatus_ID equals _estatus.Estatus_ID

                                 where 
                                 _estatus.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Numero_Parte.Nombre) ? _numero_parte.Nombre.ToLower().Contains(Obj_Numero_Parte.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Numero_Partes_Negocio
                                 {
                                     Numero_Parte_ID = _numero_parte.Numero_Parte_ID,
                                     Nombre = _numero_parte.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _numero_parte.Estatus_ID,
                                     Observaciones = _numero_parte.Observaciones,
                                 }).OrderByDescending(u => u.Numero_Parte_ID);



                    foreach (var p in numero_parte)
                        Lista_areas.Add((Cls_Cat_Numero_Partes_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_areas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }


    }
}

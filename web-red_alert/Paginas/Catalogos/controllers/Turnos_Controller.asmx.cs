﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Turnos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Turnos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta del turno.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Turnos_Negocio Obj_Turnos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Turnos = JsonMapper.ToObject<Cls_Cat_Turnos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _turnos = new Cat_Turnos();
                    _turnos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _turnos.Estatus_ID = Obj_Turnos.Estatus_ID;
                    _turnos.Nombre = Obj_Turnos.Nombre;
                    _turnos.Observaciones = Obj_Turnos.Observaciones;
                    _turnos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _turnos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Turnos.Add(_turnos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos del turno seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Turnos_Negocio Obj_Turnos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update data";
                Obj_Turnos = JsonMapper.ToObject<Cls_Cat_Turnos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _turnos = dbContext.Cat_Turnos.Where(u => u.Turno_ID == Obj_Turnos.Turno_ID).First();

                    _turnos.Estatus_ID = Obj_Turnos.Estatus_ID;
                    _turnos.Nombre = Obj_Turnos.Nombre;
                    _turnos.Observaciones = Obj_Turnos.Observaciones;
                    _turnos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _turnos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Turnos_Negocio Obj_Turnos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Turnos = JsonMapper.ToObject<Cls_Cat_Turnos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _turnos = dbContext.Cat_Turnos.Where(u => u.Turno_ID == Obj_Turnos.Turno_ID).First();
                    dbContext.Cat_Turnos.Remove(_turnos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _turno = dbContext.Cat_Turnos.Where(u => u.Turno_ID == Obj_Turnos.Turno_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _turno.Estatus_ID = _estatus.Estatus_ID;
                            _turno.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _turno.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del turno.
        /// <returns>Listado de los turnos filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Turnos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Turnos_Negocio Obj_Turnos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Turnos_Negocio> Lista_Empaques = new List<Cls_Cat_Turnos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Turnos = JsonMapper.ToObject<Cls_Cat_Turnos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _turnos = (from _turno in dbContext.Cat_Turnos
                                  join _estatus in dbContext.Apl_Estatus on _turno.Estatus_ID equals _estatus.Estatus_ID

                                  where _turno.Turno_ID.Equals(Obj_Turnos.Turno_ID) ||
                                  _turno.Nombre.Equals(Obj_Turnos.Nombre)

                                  select new Cls_Cat_Turnos_Negocio
                                  {
                                      Turno_ID = _turno.Turno_ID,
                                      Nombre = _turno.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_turnos.Any())
                    {
                        if (Obj_Turnos.Turno_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Turnos.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _turnos.Where(u => u.Turno_ID == Obj_Turnos.Turno_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Turnos.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de los turnos.
        /// </summary>
        /// <returns>Listado serializado con los turnos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Turnos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Turnos_Negocio Obj_Turnos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Turnos_Negocio> Lista_turnos = new List<Cls_Cat_Turnos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Turnos = JsonMapper.ToObject<Cls_Cat_Turnos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Turnos = (from _turnos in dbContext.Cat_Turnos
                                 join _estatus in dbContext.Apl_Estatus on _turnos.Estatus_ID equals _estatus.Estatus_ID

                                 where _turnos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Turnos.Nombre) ? _turnos.Nombre.ToLower().Contains(Obj_Turnos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Turnos_Negocio
                                 {
                                     Turno_ID = _turnos.Turno_ID,
                                     Nombre = _turnos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _turnos.Estatus_ID,
                                     Empresa_ID = _turnos.Empresa_ID,
                                     Observaciones = _turnos.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Turnos)
                        Lista_turnos.Add((Cls_Cat_Turnos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_turnos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

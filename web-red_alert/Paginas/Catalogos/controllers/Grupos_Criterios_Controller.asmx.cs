﻿using datos_red_alert;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Grupos_Criterios_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Grupos_Criterios_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta del grupo.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Grupos_Criterios_Negocio Obj_Grupos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Grupos = JsonMapper.ToObject<Cls_Cat_Grupos_Criterios_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _grupos = new Cat_Grupos_Criterios();
                    _grupos.Estatus_ID = Obj_Grupos.Estatus_ID;
                    _grupos.Nombre = Obj_Grupos.Nombre;
                    _grupos.Observaciones = Obj_Grupos.Observaciones;
                    _grupos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _grupos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Grupos_Criterios.Add(_grupos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la grupo seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Grupos_Criterios_Negocio Obj_Grupos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update data";
                Obj_Grupos = JsonMapper.ToObject<Cls_Cat_Grupos_Criterios_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _grupos = dbContext.Cat_Grupos_Criterios.Where(u => u.Grupo_ID == Obj_Grupos.Grupo_ID).First();

                    _grupos.Estatus_ID = Obj_Grupos.Estatus_ID;
                    _grupos.Nombre = Obj_Grupos.Nombre;
                    _grupos.Observaciones = Obj_Grupos.Observaciones;
                    _grupos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _grupos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Grupos_Criterios_Negocio Obj_Grupos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Grupos = JsonMapper.ToObject<Cls_Cat_Grupos_Criterios_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _grupos = dbContext.Cat_Grupos_Criterios.Where(u => u.Grupo_ID == Obj_Grupos.Grupo_ID).First();
                    dbContext.Cat_Grupos_Criterios.Remove(_grupos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _grupo = dbContext.Cat_Grupos_Criterios.Where(u => u.Grupo_ID == Obj_Grupos.Grupo_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _grupo.Estatus_ID = _estatus.Estatus_ID;
                            _grupo.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _grupo.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la grupo.
        /// <returns>Listado de las grupos  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Grupos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Grupos_Criterios_Negocio Obj_Grupos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Grupos_Criterios_Negocio> Lista_Empaques = new List<Cls_Cat_Grupos_Criterios_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Grupos = JsonMapper.ToObject<Cls_Cat_Grupos_Criterios_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _grupos = (from _grupo in dbContext.Cat_Grupos_Criterios
                                   join _estatus in dbContext.Apl_Estatus on _grupo.Estatus_ID equals _estatus.Estatus_ID

                                   where _grupo.Grupo_ID.Equals(Obj_Grupos.Grupo_ID) ||
                                   _grupo.Nombre.Equals(Obj_Grupos.Nombre)

                                   select new Cls_Cat_Grupos_Criterios_Negocio
                                   {
                                       Grupo_ID = _grupo.Grupo_ID,
                                       Nombre = _grupo.Nombre,
                                       Estatus = _estatus.Estatus
                                   }).OrderByDescending(u => u.Nombre);

                    if (_grupos.Any())
                    {
                        if (Obj_Grupos.Grupo_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Grupos.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _grupos.Where(u => u.Grupo_ID == Obj_Grupos.Grupo_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Grupos.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las grupos.
        /// </summary>
        /// <returns>Listado serializado con las grupos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Grupos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Grupos_Criterios_Negocio Obj_Grupos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Grupos_Criterios_Negocio> Lista_grupos = new List<Cls_Cat_Grupos_Criterios_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Grupos = JsonMapper.ToObject<Cls_Cat_Grupos_Criterios_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Grupos = (from _grupos in dbContext.Cat_Grupos_Criterios
                                  join _estatus in dbContext.Apl_Estatus on _grupos.Estatus_ID equals _estatus.Estatus_ID

                                  where _estatus.Estatus != "DELETED" &&
                                  (!string.IsNullOrEmpty(Obj_Grupos.Nombre) ? _grupos.Nombre.ToLower().Contains(Obj_Grupos.Nombre.ToLower()) : true)

                                  select new Cls_Cat_Grupos_Criterios_Negocio
                                  {
                                      Grupo_ID = _grupos.Grupo_ID,
                                      Nombre = _grupos.Nombre,
                                      Estatus = _estatus.Estatus,
                                      Estatus_ID = _grupos.Estatus_ID,
                                      Observaciones = _grupos.Observaciones,
                                  }).OrderByDescending(u => u.Nombre);

                    foreach (var p in Grupos)
                        Lista_grupos.Add((Cls_Cat_Grupos_Criterios_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_grupos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };

                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

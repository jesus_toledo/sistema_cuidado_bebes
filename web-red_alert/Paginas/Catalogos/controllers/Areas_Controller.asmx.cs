﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using datos_red_alert;
using LitJson;
using System.Web.Script.Services;

using web_red_alert.Models.Negocio;
using web_red_alert.Models.Ayudante;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Areas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Areas_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Areas_Negocio Obj_Areas = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Areas = JsonMapper.ToObject<Cls_Cat_Areas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _areas = new Cat_Areas();
                    _areas.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _areas.Estatus_ID = Obj_Areas.Estatus_ID;
                    _areas.Nombre = Obj_Areas.Nombre;
                    _areas.Observaciones = Obj_Areas.Observaciones;
                    _areas.Usuario_Creo = Cls_Sesiones.Usuario;
                    _areas.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Areas.Add(_areas);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Areas_Negocio Obj_Areas = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Areas = JsonMapper.ToObject<Cls_Cat_Areas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _areas = dbContext.Cat_Areas.Where(u => u.Area_ID == Obj_Areas.Area_ID).First();

                    _areas.Estatus_ID = Obj_Areas.Estatus_ID;
                    _areas.Nombre = Obj_Areas.Nombre;
                    _areas.Observaciones = Obj_Areas.Observaciones;
                    _areas.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _areas.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Areas_Negocio Obj_Areas = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Areas = JsonMapper.ToObject<Cls_Cat_Areas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _areas = dbContext.Cat_Areas.Where(u => u.Area_ID == Obj_Areas.Area_ID).First();
                    dbContext.Cat_Areas.Remove(_areas);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Red_AlertEntities())
                        {
                            var _area = dbContext.Cat_Areas.Where(u => u.Area_ID == Obj_Areas.Area_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _area.Estatus_ID = _estatus.Estatus_ID;
                            _area.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _area.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Areas_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Areas_Negocio Obj_Areas = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Areas_Negocio> Lista_Empaques = new List<Cls_Cat_Areas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Areas = JsonMapper.ToObject<Cls_Cat_Areas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _areas = (from _area in dbContext.Cat_Areas
                                     join _estatus in dbContext.Apl_Estatus on _area.Estatus_ID equals _estatus.Estatus_ID

                                     where _area.Area_ID.Equals(Obj_Areas.Area_ID) ||
                                     _area.Nombre.Equals(Obj_Areas.Nombre)

                                     select new Cls_Cat_Areas_Negocio
                                     {
                                         Area_ID = _area.Area_ID,
                                         Nombre = _area.Nombre,
                                         Estatus = _estatus.Estatus
                                     }).OrderByDescending(u => u.Nombre);

                    if (_areas.Any())
                    {
                        if (Obj_Areas.Area_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Areas.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _areas.Where(u => u.Area_ID == Obj_Areas.Area_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Areas.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Areas_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Areas_Negocio Obj_Areas = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Areas_Negocio> Lista_areas = new List<Cls_Cat_Areas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Areas = JsonMapper.ToObject<Cls_Cat_Areas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Areas = (from _areas in dbContext.Cat_Areas
                                    join _estatus in dbContext.Apl_Estatus on _areas.Estatus_ID equals _estatus.Estatus_ID

                                    where _areas.Empresa_ID.Equals(empresa_id) &&
                                    _estatus.Estatus != "DELETED" &&
                                    (!string.IsNullOrEmpty(Obj_Areas.Nombre) ? _areas.Nombre.ToLower().Contains(Obj_Areas.Nombre.ToLower()) : true)

                                    select new Cls_Cat_Areas_Negocio
                                    {
                                        Area_ID = _areas.Area_ID,
                                        Nombre = _areas.Nombre,
                                        Estatus = _estatus.Estatus,
                                        Estatus_ID = _areas.Estatus_ID,
                                        Empresa_ID = _areas.Empresa_ID,
                                        Observaciones = _areas.Observaciones,
                                    }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Areas)
                        Lista_areas.Add((Cls_Cat_Areas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_areas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };


                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

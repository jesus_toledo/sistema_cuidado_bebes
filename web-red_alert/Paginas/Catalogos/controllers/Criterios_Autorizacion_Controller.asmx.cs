﻿using datos_red_alert;
using LitJson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_red_alert.Models.Ayudante;
using web_red_alert.Models.Negocio;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Criterios_Autorizacion_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Criterios_Autorizacion_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = null;
            List<Cls_Cat_Responsables_Autorizacion_Negocio> lst_Responsables = new List<Cls_Cat_Responsables_Autorizacion_Negocio>();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);
                lst_Responsables = JsonConvert.DeserializeObject<List<Cls_Cat_Responsables_Autorizacion_Negocio>>(Obj_Criterios.Detalles);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _criterio_autorizacion_id = dbContext.Cat_Criterios_Autorizacion.DefaultIfEmpty().Max(x => x == null ? 0 : x.Criterio_Autorizacion_ID);
                    
                    var _criterios = new Cat_Criterios_Autorizacion();
                    _criterios.Criterio_Autorizacion_ID = _criterio_autorizacion_id + 1;
                    _criterios.Nombre = Obj_Criterios.Nombre;
                    _criterios.Nodo_ID = Obj_Criterios.Nodo_ID;
                    _criterios.Tipo_Criterio = Obj_Criterios.Tipo_Criterio;
                    _criterios.Grupo_ID = Obj_Criterios.Grupo_ID;
                    _criterios.Estatus_ID = Obj_Criterios.Estatus_ID;
                    _criterios.Usuario_Creo = !String.IsNullOrEmpty(Cls_Sesiones.Usuario) ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _criterios.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Criterios_Autorizacion.Add(_criterios);

                    foreach(var Detalles in lst_Responsables)
                    {
                        Cat_Responsables_Autorizacion det_responsable = new Cat_Responsables_Autorizacion();
                        det_responsable.Criterio_Autorizacion_ID = _criterio_autorizacion_id + 1;
                        det_responsable.No_Empleado = Detalles.No_Empleado;
                        det_responsable.Nombre = Detalles.Nombre;
                        det_responsable.Email = Detalles.Email;
                        det_responsable.Telefono = Detalles.Telefono;
                        det_responsable.Usuario_Creo = !String.IsNullOrEmpty(Cls_Sesiones.Usuario) ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                        det_responsable.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                        dbContext.Cat_Responsables_Autorizacion.Add(det_responsable);
                    }

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. < br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = null;
            List<Cls_Cat_Responsables_Autorizacion_Negocio> lst_Responsables = new List<Cls_Cat_Responsables_Autorizacion_Negocio>();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update data";
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);
                lst_Responsables = JsonConvert.DeserializeObject<List<Cls_Cat_Responsables_Autorizacion_Negocio>>(Obj_Criterios.Detalles);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _criterios = dbContext.Cat_Criterios_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID).First();
                    
                    _criterios.Nombre = Obj_Criterios.Nombre;
                    _criterios.Nodo_ID = Obj_Criterios.Nodo_ID;
                    _criterios.Estatus_ID = Obj_Criterios.Estatus_ID;
                    _criterios.Tipo_Criterio = Obj_Criterios.Tipo_Criterio;
                    _criterios.Grupo_ID = Obj_Criterios.Grupo_ID;
                    _criterios.Usuario_Modifico = !String.IsNullOrEmpty(Cls_Sesiones.Usuario) ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                    _criterios.Fecha_Modifico = new DateTime?(DateTime.Now);

                    var _detalles_responsables = dbContext.Cat_Responsables_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID).ToList();
                    foreach (var _detalle in _detalles_responsables)
                    {
                        dbContext.Cat_Responsables_Autorizacion.Remove(_detalle);
                    }

                    foreach(var Detalles in lst_Responsables)
                    {
                        Cat_Responsables_Autorizacion det_responsable = new Cat_Responsables_Autorizacion();
                        det_responsable.Criterio_Autorizacion_ID = Obj_Criterios.Criterio_Autorizacion_ID;
                        det_responsable.No_Empleado = Detalles.No_Empleado;
                        det_responsable.Nombre = Detalles.Nombre;
                        det_responsable.Email = Detalles.Email;
                        det_responsable.Telefono = Detalles.Telefono;
                        det_responsable.Usuario_Creo = !String.IsNullOrEmpty(Cls_Sesiones.Usuario) ? Cls_Sesiones.Usuario : Cls_Sesiones.Empleado;
                        det_responsable.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                        dbContext.Cat_Responsables_Autorizacion.Add(det_responsable);
                    }

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than the one set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp;Please check that you are not entering duplicate data.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove data";
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _responsables = dbContext.Cat_Responsables_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID).ToList();
                    foreach (var _detalle in _responsables)
                    {
                        dbContext.Cat_Responsables_Autorizacion.Remove(_detalle);
                    }
                    var _criterios = dbContext.Cat_Criterios_Autorizacion.Where(u => u.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID).First();
                    
                    dbContext.Cat_Criterios_Autorizacion.Remove(_criterios);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                }
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Criterios_Autorizacion_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Criterios_Autorizacion_Negocio> Lista_Empaques = new List<Cls_Cat_Criterios_Autorizacion_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _criterios = (from _criterio in dbContext.Cat_Criterios_Autorizacion

                                  where 
                                  _criterio.Nombre.ToUpper() == Obj_Criterios.Nombre.ToUpper() &&
                                  _criterio.Tipo_Criterio.ToUpper() == Obj_Criterios.Tipo_Criterio.ToUpper() &&
                                  _criterio.Grupo_ID == Obj_Criterios.Grupo_ID

                                  select new Cls_Cat_Criterios_Autorizacion_Negocio
                                  {
                                      Criterio_Autorizacion_ID= _criterio.Criterio_Autorizacion_ID,
                                      Nombre = _criterio.Nombre,
                                      Tipo_Criterio = _criterio.Tipo_Criterio,
                                      Grupo_ID = _criterio.Grupo_ID
                                  }).OrderByDescending(u => u.Nombre);

                    if (_criterios.Any())
                    {
                        if (Obj_Criterios.Criterio_Autorizacion_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Criterios.Nombre))
                                Mensaje.Mensaje = "The name entered is already registered.";
                        }
                        else
                        {
                            var item_edit = _criterios.Where(u => u.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Criterios.Nombre))
                                    Mensaje.Mensaje = "The name entered is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de los respobsables asignados.
        /// </summary>
        /// <returns>Listado serializado con el resultado</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Responsables_Asignados(string jsonObject)
        {
            Cls_Cat_Responsables_Autorizacion_Negocio Obj_Responsables = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Responsables = JsonMapper.ToObject<Cls_Cat_Responsables_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Responsables = (from _responsables in dbContext.Cat_Responsables_Autorizacion
                                     where _responsables.No_Empleado == Obj_Responsables.No_Empleado
                                     
                                     select new Cls_Cat_Responsables_Autorizacion_Negocio
                                     {
                                         Responsable_ID = _responsables.Responsable_ID,
                                         Nombre = _responsables.Nombre
                                     }).OrderByDescending(u => u.Responsable_ID);

                    if (Responsables.Any())
                        Mensaje.Mensaje = (true).ToString();
                    else
                        Mensaje.Mensaje = (false).ToString();

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Criterios_Autorizacion_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Criterios_Autorizacion_Negocio> Lista_criterios = new List<Cls_Cat_Criterios_Autorizacion_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ACTIVE").First();

                    var Criterios = (from _criterios in dbContext.Cat_Criterios_Autorizacion
                                     join _grupos in dbContext.Cat_Grupos_Criterios on _criterios.Grupo_ID equals _grupos.Grupo_ID
                                    where (!string.IsNullOrEmpty(Obj_Criterios.Nombre) ? _criterios.Nombre.ToLower().Contains(Obj_Criterios.Nombre.ToLower()) : true)
                                    && _criterios.Estatus_ID == _estatus.Estatus_ID
                                   

                                 select new Cls_Cat_Criterios_Autorizacion_Negocio
                                 {
                                     Criterio_Autorizacion_ID = _criterios.Criterio_Autorizacion_ID,
                                     Nombre = _criterios.Nombre,
                                     Nodo_ID = _criterios.Nodo_ID,
                                     Grupo = _grupos.Nombre,
                                     Grupo_ID = _criterios.Grupo_ID,
                                     Estatus_ID = _criterios.Estatus_ID,
                                     Estatus = _estatus.Estatus,
                                     Tipo_Criterio = _criterios.Tipo_Criterio,
                                     
                                 }).OrderByDescending(u => u.Criterio_Autorizacion_ID);
                    
                    foreach (var p in Criterios)
                        Lista_criterios.Add((Cls_Cat_Criterios_Autorizacion_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_criterios);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los grupos.
        /// </summary>
        /// <returns>Listado serializado de los grupos</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Grupos()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Grupos = from _empresas in dbContext.Cat_Grupos_Criterios
                                  select new { _empresas.Nombre, _empresas.Grupo_ID };

                    Json_Resultado = JsonMapper.ToJson(Grupos.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los datos del empleado.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Datos_Empleado(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = new Cls_Cat_Empleados_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_empleado = (from _empleado in dbContext.Cat_Empleados
                                         join _empresa in dbContext.Apl_Empresas on _empleado.Empresa_ID equals _empresa.Empresa_ID
                                         where _empleado.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _empleado.Estatus == "ACTIVE"
                                         && _empleado.Empleado_ID == Obj_Empleados.Empleado_ID

                                         select new Cls_Cat_Empleados_Negocio
                                         {
                                             Empleado_ID = _empleado.Empleado_ID,
                                             No_Empleado = _empleado.No_Empleado,
                                             Planta = _empleado.Planta,
                                             Email = _empleado.Email,
                                             Empresa_ID = _empleado.Empresa_ID,
                                             Empresa = _empresa.Clave
                                         }).OrderBy(u => u.No_Empleado);

                    foreach (var p in _lst_empleado)
                        Lista_Empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los departamentos.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Departamentos()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Departamentos> Lista_departamentos = new List<Cat_Departamentos>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ACTIVE").First();

                    var Departamentos = from _departamentos in dbContext.Cat_Departamentos
                                        where _departamentos.Estatus_ID == _estatus.Estatus_ID
                                  select new { _departamentos.Nombre, _departamentos.Departamento_ID };

                    Json_Resultado = JsonMapper.ToJson(Departamentos.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los empleados.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Empleados()
        {
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Empleados = (from _empleados in dbContext.Cat_Empleados
                                     where _empleados.Estatus != "INACTIVO"
                                    select new Cls_Cat_Empleados_Negocio
                                    {
                                        Nombre=_empleados.Nombre + " " + _empleados.Apellidos,
                                        No_Empleado = _empleados.No_Empleado
                                    }).OrderBy(u=>u.Nombre);

                    foreach (var p in Empleados)
                        Lista_empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los empleados.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Criterios_Autorizacion()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_criterios = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var Criterios = from _criterios in dbContext.Cat_Criterios_Autorizacion
                                    join _grupos in dbContext.Cat_Grupos_Criterios on _criterios.Grupo_ID equals _grupos.Grupo_ID
                                  select new Cls_Cat_Criterios_Autorizacion_Negocio
                                  {
                                    Nombre = _criterios.Tipo_Criterio + "-" + _grupos.Nombre + "-" + _criterios.Nombre,
                                    Criterio_Autorizacion_ID =_criterios.Criterio_Autorizacion_ID
                                  };

                    Json_Resultado = JsonMapper.ToJson(Criterios.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Datos_Criterios(string jsonObject)
        {
            Cls_Cat_Criterios_Autorizacion_Negocio Obj_Criterios = new Cls_Cat_Criterios_Autorizacion_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Criterios_Autorizacion_Negocio> Lista_Criterios = new List<Cls_Cat_Criterios_Autorizacion_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Criterios = JsonMapper.ToObject<Cls_Cat_Criterios_Autorizacion_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_criterio = (from _criterio in dbContext.Cat_Criterios_Autorizacion
                                         where
                                         _criterio.Criterio_Autorizacion_ID == Obj_Criterios.Criterio_Autorizacion_ID

                                         select new Cls_Cat_Criterios_Autorizacion_Negocio
                                         {
                                             Criterio_Autorizacion_ID = _criterio.Criterio_Autorizacion_ID,
                                             Tipo_Criterio = _criterio.Tipo_Criterio,
                                             Grupo_ID = _criterio.Grupo_ID
                                         }).OrderBy(u => u.Criterio_Autorizacion_ID);

                    foreach (var p in _lst_criterio)
                        Lista_Criterios.Add((Cls_Cat_Criterios_Autorizacion_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Criterios);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_lista_responsables(string jsonObject)
        {
            Cls_Cat_Responsables_Autorizacion_Negocio Obj_Responsable = new Cls_Cat_Responsables_Autorizacion_Negocio();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Cat_Responsables_Autorizacion_Negocio> Lista_responsables = new List<Cls_Cat_Responsables_Autorizacion_Negocio>();

            try
            {
                Obj_Responsable = JsonMapper.ToObject<Cls_Cat_Responsables_Autorizacion_Negocio>(jsonObject);
                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_responsables= (from _responsables in dbContext.Cat_Responsables_Autorizacion
                                      where _responsables.Criterio_Autorizacion_ID.Equals(Obj_Responsable.Criterio_Autorizacion_ID)
                                         select new Cls_Cat_Responsables_Autorizacion_Negocio
                                         {
                                             Responsable_ID = _responsables.Responsable_ID,
                                             Nombre = _responsables.Nombre,
                                             No_Empleado = _responsables.No_Empleado,
                                             Email = _responsables.Email,
                                             Telefono = _responsables.Telefono
                                         }).OrderBy(u => u.Nombre).ToList();

                    foreach (var p in _lst_responsables)
                        Lista_responsables.Add(p);
                    Json_Resultado = JsonMapper.ToJson(Lista_responsables);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar el email de un empleado
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Email(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleado = new Cls_Cat_Empleados_Negocio();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Cat_Empleados_Negocio> Lista_empleados = new List<Cls_Cat_Empleados_Negocio>();

            try
            {
                Obj_Empleado = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new AAM_Red_AlertEntities())
                {
                    var _lst_empleados = (from _empleados in dbContext.Cat_Empleados
                                         where _empleados.No_Empleado == Obj_Empleado.No_Empleado

                                         select new Cls_Cat_Empleados_Negocio
                                         {
                                             No_Empleado = _empleados.No_Empleado,
                                             Email = _empleados.Email
                                         }).OrderBy(u => u.No_Empleado);

                    foreach (var p in _lst_empleados)
                        Lista_empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Sitios_Clientes_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Sitios_Clientes_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta del sitio cliente.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _sitios_clientes = new Cat_Sitios_Clientes();
                    _sitios_clientes.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _sitios_clientes.Cliente_ID = Obj_Sitios_Clientes.Cliente_ID;
                    _sitios_clientes.Estatus_ID = Obj_Sitios_Clientes.Estatus_ID;
                    _sitios_clientes.Nombre = Obj_Sitios_Clientes.Nombre;
                    _sitios_clientes.Observaciones = Obj_Sitios_Clientes.Observaciones;
                    _sitios_clientes.Usuario_Creo = Cls_Sesiones.Usuario;
                    _sitios_clientes.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Sitios_Clientes.Add(_sitios_clientes);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos del sitio cliente seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _sitios_clientes = dbContext.Cat_Sitios_Clientes.Where(u => u.Sitio_Cliente_ID == Obj_Sitios_Clientes.Sitio_Cliente_ID).First();

                    _sitios_clientes.Cliente_ID = Obj_Sitios_Clientes.Cliente_ID;
                    _sitios_clientes.Estatus_ID = Obj_Sitios_Clientes.Estatus_ID;
                    _sitios_clientes.Nombre = Obj_Sitios_Clientes.Nombre;
                    _sitios_clientes.Observaciones = Obj_Sitios_Clientes.Observaciones;
                    _sitios_clientes.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _sitios_clientes.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _sitios_clientes = dbContext.Cat_Sitios_Clientes.Where(u => u.Sitio_Cliente_ID == Obj_Sitios_Clientes.Sitio_Cliente_ID).First();
                    dbContext.Cat_Sitios_Clientes.Remove(_sitios_clientes);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Cambios_ProcesosEntities())
                        {
                            var _sitio_cliente = dbContext.Cat_Sitios_Clientes.Where(u => u.Sitio_Cliente_ID == Obj_Sitios_Clientes.Sitio_Cliente_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _sitio_cliente.Estatus_ID = _estatus.Estatus_ID;
                            _sitio_cliente.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _sitio_cliente.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del sitio cliente.
        /// <returns>Listado de los sitios clientes  filtrados por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Sitios_Clientes_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Sitios_Clientes_Negocio> Lista_Empaques = new List<Cls_Cat_Sitios_Clientes_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _sitios_clientes = (from _sitio_cliente in dbContext.Cat_Sitios_Clientes
                                  join _estatus in dbContext.Apl_Estatus on _sitio_cliente.Estatus_ID equals _estatus.Estatus_ID

                                  where _sitio_cliente.Sitio_Cliente_ID.Equals(Obj_Sitios_Clientes.Sitio_Cliente_ID) ||
                                  _sitio_cliente.Nombre.Equals(Obj_Sitios_Clientes.Nombre)

                                  select new Cls_Cat_Sitios_Clientes_Negocio
                                  {
                                      Sitio_Cliente_ID = _sitio_cliente.Sitio_Cliente_ID,
                                      Nombre = _sitio_cliente.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_sitios_clientes.Any())
                    {
                        if (Obj_Sitios_Clientes.Sitio_Cliente_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Sitios_Clientes.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _sitios_clientes.Where(u => u.Sitio_Cliente_ID == Obj_Sitios_Clientes.Sitio_Cliente_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Sitios_Clientes.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de los sitios clientes.
        /// </summary>
        /// <returns>Listado serializado con los sitios clientes según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Sitios_Clientes_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Sitios_Clientes_Negocio> Lista_sitios_clientes = new List<Cls_Cat_Sitios_Clientes_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Sitios_Clientes = (from _sitios_clientes in dbContext.Cat_Sitios_Clientes
                                 join _estatus in dbContext.Apl_Estatus on _sitios_clientes.Estatus_ID equals _estatus.Estatus_ID
                                 join _clientes in dbContext.Cat_Clientes on _sitios_clientes.Cliente_ID equals _clientes.Cliente_ID

                                 where _sitios_clientes.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Sitios_Clientes.Nombre) ? _sitios_clientes.Nombre.ToLower().Contains(Obj_Sitios_Clientes.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Sitios_Clientes_Negocio
                                 {
                                     Sitio_Cliente_ID = _sitios_clientes.Sitio_Cliente_ID,
                                     Nombre = _sitios_clientes.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _sitios_clientes.Estatus_ID,
                                     Empresa_ID = _sitios_clientes.Empresa_ID,
                                     Cliente = _clientes.Nombre,
                                     Cliente_ID = _sitios_clientes.Cliente_ID,
                                     Observaciones = _sitios_clientes.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);
                    

                    foreach (var p in Sitios_Clientes)
                        Lista_sitios_clientes.Add((Cls_Cat_Sitios_Clientes_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_sitios_clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar los clientes.
        /// </summary>
        /// <returns>Listado serializado de los clientes</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Clientes()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Clientes> Lista_clientes = new List<Cat_Clientes>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Clientes = from _clientes in dbContext.Cat_Clientes
                                  select new { _clientes.Nombre, _clientes.Cliente_ID };

                    Json_Resultado = JsonMapper.ToJson(Clientes.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

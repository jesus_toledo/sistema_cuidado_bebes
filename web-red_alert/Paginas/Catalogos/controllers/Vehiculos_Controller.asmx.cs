﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Vehiculos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Vehiculos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la vehiculo.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _vehiculos = new Cat_Vehiculos();
                    _vehiculos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _vehiculos.Sitio_Cliente_ID = Obj_Vehiculos.Sitio_Cliente_ID;
                    _vehiculos.Estatus_ID = Obj_Vehiculos.Estatus_ID;
                    _vehiculos.Nombre = Obj_Vehiculos.Nombre;
                    _vehiculos.Observaciones = Obj_Vehiculos.Observaciones;
                    _vehiculos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _vehiculos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Vehiculos.Add(_vehiculos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la vehiculo seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _vehiculos = dbContext.Cat_Vehiculos.Where(u => u.Vehiculo_ID == Obj_Vehiculos.Vehiculo_ID).First();

                    _vehiculos.Estatus_ID = Obj_Vehiculos.Estatus_ID;
                    _vehiculos.Nombre = Obj_Vehiculos.Nombre;
                    _vehiculos.Observaciones = Obj_Vehiculos.Observaciones;
                    _vehiculos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _vehiculos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _vehiculos = dbContext.Cat_Vehiculos.Where(u => u.Vehiculo_ID == Obj_Vehiculos.Vehiculo_ID).First();
                    dbContext.Cat_Vehiculos.Remove(_vehiculos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Cambios_ProcesosEntities())
                        {
                            var _vehiculo = dbContext.Cat_Vehiculos.Where(u => u.Vehiculo_ID == Obj_Vehiculos.Vehiculo_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _vehiculo.Estatus_ID = _estatus.Estatus_ID;
                            _vehiculo.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _vehiculo.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda del vehiculo.
        /// <returns>Listado de los vehiculos filtrados por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Vehiculos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Vehiculos_Negocio> Lista_Empaques = new List<Cls_Cat_Vehiculos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _vehiculos = (from _vehiculo in dbContext.Cat_Vehiculos
                                  join _estatus in dbContext.Apl_Estatus on _vehiculo.Estatus_ID equals _estatus.Estatus_ID

                                  where _vehiculo.Vehiculo_ID.Equals(Obj_Vehiculos.Vehiculo_ID) ||
                                  _vehiculo.Nombre.Equals(Obj_Vehiculos.Nombre)

                                  select new Cls_Cat_Vehiculos_Negocio
                                  {
                                      Vehiculo_ID = _vehiculo.Vehiculo_ID,
                                      Nombre = _vehiculo.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_vehiculos.Any())
                    {
                        if (Obj_Vehiculos.Vehiculo_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Vehiculos.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _vehiculos.Where(u => u.Vehiculo_ID == Obj_Vehiculos.Vehiculo_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Vehiculos.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de los vehiculos.
        /// </summary>
        /// <returns>Listado serializado con los vehiculos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Vehiculos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Vehiculos_Negocio> Lista_vehiculos = new List<Cls_Cat_Vehiculos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Vehiculos = (from _vehiculos in dbContext.Cat_Vehiculos
                                 join _estatus in dbContext.Apl_Estatus on _vehiculos.Estatus_ID equals _estatus.Estatus_ID
                                 join _sitios in dbContext.Cat_Sitios_Clientes on _vehiculos.Sitio_Cliente_ID equals _sitios.Sitio_Cliente_ID

                                 where _vehiculos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Vehiculos.Nombre) ? _vehiculos.Nombre.ToLower().Contains(Obj_Vehiculos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Vehiculos_Negocio
                                 {
                                     Vehiculo_ID = _vehiculos.Vehiculo_ID,
                                     Nombre = _vehiculos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _vehiculos.Estatus_ID,
                                     Empresa_ID = _vehiculos.Empresa_ID,
                                     Sitio_Cliente_ID = _vehiculos.Sitio_Cliente_ID,
                                     Sitio_Cliente = _sitios.Nombre,
                                     Observaciones = _vehiculos.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);
                    

                    foreach (var p in Vehiculos)
                        Lista_vehiculos.Add((Cls_Cat_Vehiculos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_vehiculos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los sitios clientes.
        /// </summary>
        /// <returns>Listado serializado de los sitios clientes</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Sitios_Clientes()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Sitios_Clientes> Lista_sitios = new List<Cat_Sitios_Clientes>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Sitios = from _sitios in dbContext.Cat_Sitios_Clientes
                                  select new { _sitios.Nombre, _sitios.Sitio_Cliente_ID };

                    Json_Resultado = JsonMapper.ToJson(Sitios.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

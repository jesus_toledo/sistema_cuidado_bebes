﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Referencia_Productos_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Referencia_Productos_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la referencia_producto.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Referencia_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Referencia_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _referencia_productos = new Cat_Referencia_Productos();
                    _referencia_productos.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _referencia_productos.Estatus_ID = Obj_Referencia_Productos.Estatus_ID;
                    _referencia_productos.Cliente_ID = Obj_Referencia_Productos.Cliente_ID;
                    _referencia_productos.Nombre = Obj_Referencia_Productos.Nombre;
                    _referencia_productos.Observaciones = Obj_Referencia_Productos.Observaciones;
                    _referencia_productos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _referencia_productos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Referencia_Productos.Add(_referencia_productos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la referencia_producto seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Referencia_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Referencia_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _referencia_productos = dbContext.Cat_Referencia_Productos.Where(u => u.Referencia_Producto_ID == Obj_Referencia_Productos.Referencia_Producto_ID).First();

                    _referencia_productos.Estatus_ID = Obj_Referencia_Productos.Estatus_ID;
                    _referencia_productos.Cliente_ID = Obj_Referencia_Productos.Cliente_ID;
                    _referencia_productos.Nombre = Obj_Referencia_Productos.Nombre;
                    _referencia_productos.Observaciones = Obj_Referencia_Productos.Observaciones;
                    _referencia_productos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _referencia_productos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Referencia_Productos = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_Referencia_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _referencia_productos = dbContext.Cat_Referencia_Productos.Where(u => u.Referencia_Producto_ID == Obj_Referencia_Productos.Referencia_Producto_ID).First();
                    dbContext.Cat_Referencia_Productos.Remove(_referencia_productos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Cambios_ProcesosEntities())
                        {
                            var _referencia_producto = dbContext.Cat_Referencia_Productos.Where(u => u.Referencia_Producto_ID == Obj_Referencia_Productos.Referencia_Producto_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _referencia_producto.Estatus_ID = _estatus.Estatus_ID;
                            _referencia_producto.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _referencia_producto.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la referencia_producto.
        /// <returns>Listado de las referencia_productos  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Referencia_Productos_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Referencia_Productos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Referencia_Productos_Negocio> Lista_Empaques = new List<Cls_Cat_Referencia_Productos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Referencia_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _referencia_productos = (from _referencia_producto in dbContext.Cat_Referencia_Productos
                                  join _estatus in dbContext.Apl_Estatus on _referencia_producto.Estatus_ID equals _estatus.Estatus_ID

                                  where _referencia_producto.Referencia_Producto_ID.Equals(Obj_Referencia_Productos.Referencia_Producto_ID) ||
                                  _referencia_producto.Nombre.Equals(Obj_Referencia_Productos.Nombre)

                                  select new Cls_Cat_Referencia_Productos_Negocio
                                  {
                                      Referencia_Producto_ID = _referencia_producto.Referencia_Producto_ID,
                                      Nombre = _referencia_producto.Nombre,
                                      Estatus = _estatus.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_referencia_productos.Any())
                    {
                        if (Obj_Referencia_Productos.Referencia_Producto_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Referencia_Productos.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _referencia_productos.Where(u => u.Referencia_Producto_ID == Obj_Referencia_Productos.Referencia_Producto_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Referencia_Productos.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las referencia_productos.
        /// </summary>
        /// <returns>Listado serializado con las referencia_productos según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Referencia_Productos_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Referencia_Productos = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Referencia_Productos_Negocio> Lista_referencia_productos = new List<Cls_Cat_Referencia_Productos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Referencia_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Referencia_Productos = (from _referencia_productos in dbContext.Cat_Referencia_Productos
                                 join _estatus in dbContext.Apl_Estatus on _referencia_productos.Estatus_ID equals _estatus.Estatus_ID
                                 join _cliente in dbContext.Cat_Clientes on _referencia_productos.Cliente_ID equals _cliente.Cliente_ID

                                 where _referencia_productos.Empresa_ID.Equals(empresa_id) &&
                                 _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Referencia_Productos.Nombre) ? _referencia_productos.Nombre.ToLower().Contains(Obj_Referencia_Productos.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Referencia_Productos_Negocio
                                 {
                                     Referencia_Producto_ID = _referencia_productos.Referencia_Producto_ID,
                                     Nombre = _referencia_productos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Estatus_ID = _referencia_productos.Estatus_ID,
                                     Empresa_ID = _referencia_productos.Empresa_ID,
                                     Cliente_ID = _referencia_productos.Cliente_ID,
                                     Cliente = _cliente.Nombre,
                                     Observaciones = _referencia_productos.Observaciones,
                                 }).OrderByDescending(u => u.Nombre);
                    

                    foreach (var p in Referencia_Productos)
                        Lista_referencia_productos.Add((Cls_Cat_Referencia_Productos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_referencia_productos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };
                    
                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        /// <summary>
        /// Método para consultar los clientes.
        /// </summary>
        /// <returns>Listado serializado de los clientes</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Clientes()
        {
            string Json_Resultado = string.Empty;
            List<Cat_Clientes> Lista_clientes = new List<Cat_Clientes>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Cliente = from _clientes in dbContext.Cat_Clientes
                                  select new { _clientes.Nombre, _clientes.Cliente_ID };

                    Json_Resultado = JsonMapper.ToJson(Cliente.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}

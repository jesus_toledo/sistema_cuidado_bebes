﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using datos_red_alert;
using LitJson;
using System.Web.Script.Services;

using web_red_alert.Models.Negocio;
using web_red_alert.Models.Ayudante;

namespace web_red_alert.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Summary description for Empleados_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Empleados_Controller : System.Web.Services.WebService
    {
        #region (Métodos)
        /// <summary>
        /// Método que realiza el alta de la area.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Insert data";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    var _empleados = new Cat_Empleados();
                    _empleados.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _empleados.No_Empleado = Obj_Empleados.No_Empleado;
                    _empleados.Nombre = Obj_Empleados.Nombre;
                    _empleados.Apellidos = Obj_Empleados.Apellidos;
                    _empleados.Nivel = Obj_Empleados.Nivel;
                    _empleados.Email = Obj_Empleados.Email;
                    _empleados.Planta = Obj_Empleados.Planta;
                    _empleados.Estatus = Obj_Empleados.Estatus;
                    _empleados.Observaciones = Obj_Empleados.Observaciones;
                    _empleados.Usuario_Creo = Cls_Sesiones.Usuario;
                    _empleados.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Empleados.Add(_empleados);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that there is no duplicate data entry.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la actualización de los datos de la area seleccionada.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Update data";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();

                    _empleado.No_Empleado = Obj_Empleados.No_Empleado;
                    _empleado.Nombre = Obj_Empleados.Nombre;
                    _empleado.Apellidos = Obj_Empleados.Apellidos;
                    _empleado.Nivel = Obj_Empleados.Nivel;
                    _empleado.Email = Obj_Empleados.Email;
                    _empleado.Planta = Obj_Empleados.Planta;
                    _empleado.Estatus = Obj_Empleados.Estatus;
                    _empleado.Observaciones = Obj_Empleados.Observaciones;
                    _empleado.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _empleado.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("String or binary data would be truncated"))
                    Mensaje.Mensaje =
                        "Any of the fields you are trying to insert have a size larger than that set in the database. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; String or binary data would be truncated";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "There are defined fields as keys that can not be duplicated. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Please check that there is no duplicate data entry.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que elimina el registro seleccionado.
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Remove";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();
                    dbContext.Cat_Empleados.Remove(_empleado);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "The operation completed without problems.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Technical report";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new ERP_EJE_CENTRALEntities())
                        {
                            var _empleado = dbContext.Cat_Empleados.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "DELETED").First();

                            _empleado.Estatus = _estatus.Estatus;
                            _empleado.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _empleado.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "The operation completed without problems.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Technical report: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de la area.
        /// <returns>Listado de las areas  filtradas por nombre</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Empleados_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validations";
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    var _empleado = (from _empleados in dbContext.Cat_Empleados
                                     where _empleados.Empleado_ID.Equals(Obj_Empleados.Empleado_ID) ||
                                  (_empleados.Nombre.Equals(Obj_Empleados.Nombre) && _empleados.Apellidos.Equals(Obj_Empleados.Apellidos) ) ||
                                  _empleados.No_Empleado.Equals(Obj_Empleados.No_Empleado)

                                  select new Cls_Cat_Empleados_Negocio
                                  {
                                      Empleado_ID = _empleados.Empleado_ID,
                                      No_Empleado= _empleados.No_Empleado,
                                      Nombre = _empleados.Nombre + " " + _empleados.Apellidos,
                                      Apellidos=_empleados.Apellidos,
                                      Nivel= _empleados.Nivel,
                                      Email= _empleados.Email,
                                      Planta= _empleados.Planta,
                                      Estatus = _empleados.Estatus
                                  }).OrderByDescending(u => u.Nombre);

                    if (_empleado.Any())
                    {
                        if (Obj_Empleados.Empleado_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Empleados.Nombre))
                                Mensaje.Mensaje = "The name and surnames entered are already registered.";
                            else if (!string.IsNullOrEmpty(Obj_Empleados.No_Empleado))
                                Mensaje.Mensaje = "Employee number is already registered.";
                        }
                        else
                        {
                            var item_edit = _empleado.Where(u => u.Empleado_ID == Obj_Empleados.Empleado_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Empleados.Nombre))
                                    Mensaje.Mensaje = "The name and surnames entered are already registered.";
                                else if (!string.IsNullOrEmpty(Obj_Empleados.No_Empleado))
                                    Mensaje.Mensaje = "Employee number is already registered.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método que realiza la búsqueda de las areas.
        /// </summary>
        /// <returns>Listado serializado con las areas según los filtros aplícados</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Empleados_Por_Filtros(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new ERP_EJE_CENTRALEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Empleados = (from _empleados in dbContext.Cat_Empleados
                                     where _empleados.Empresa_ID.Equals(empresa_id) &&
                                     _empleados.Estatus != "DELETED" &&
                                 (!string.IsNullOrEmpty(Obj_Empleados.Nombre) ? _empleados.Nombre.ToLower().Contains(Obj_Empleados.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Empleados_Negocio
                                 {
                                     Empleado_ID = _empleados.Empleado_ID,
                                     No_Empleado = _empleados.No_Empleado,
                                     Nombre = _empleados.Nombre,
                                     Empresa_ID = _empleados.Empresa_ID,
                                     Observaciones = _empleados.Observaciones,
                                     Apellidos = _empleados.Apellidos,
                                     Nivel = _empleados.Nivel,
                                     Email = _empleados.Email,
                                     Planta = _empleados.Planta,
                                     Empleado= _empleados.Nombre + " " + _empleados.Apellidos,
                                     Estatus = _empleados.Estatus
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Empleados)
                        Lista_empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los estatus.
        /// </summary>
        /// <returns>Listado serializado de los estatus</returns>
        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string Consultar_Estatus()
        //{
        //    string Json_Resultado = string.Empty;
        //    List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
        //    Cls_Mensaje Mensaje = new Cls_Mensaje();
        //    try
        //    {

        //        using (var dbContext = new ERP_EJE_CENTRALEntities())
        //        {
        //            var Estatus = from _empresas in dbContext.Apl_Estatus
        //                          select new { _empresas.Estatus, _empresas.Estatus_ID };


        //            Json_Resultado = JsonMapper.ToJson(Estatus.ToList());


        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        Mensaje.Estatus = "error";
        //        Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
        //    }
        //    return Json_Resultado;
        //}

        #endregion
    }
}
